<?php

function tml_registration_errors($errors)
{
    if (empty($_POST['first_name'])) {
        $errors->add('empty_first_name', '<strong>ERROR</strong>: Please enter your first name.');
    }
    if (empty($_POST['last_name'])) {
        $errors->add('empty_last_name', '<strong>ERROR</strong>: Please enter your last name.');
    }
    if (empty($_POST['nazione'])) {
        $errors->add('empty_nazione', '<strong>ERROR</strong>: Please enter your country.');
    }
    if (empty($_POST['professione'])) {
        $errors->add('empty_professione', '<strong>ERROR</strong>: Please enter your job.');
    }

    $errors->remove('empty_username');

    return $errors;
}

add_filter('registration_errors', 'tml_registration_errors');

function tml_user_register($user_id)
{
    if ( ! empty($_POST['first_name'])) {
        update_user_meta($user_id, 'first_name', $_POST['first_name']);
    }
    if ( ! empty($_POST['last_name'])) {
        update_user_meta($user_id, 'last_name', $_POST['last_name']);
    }
    if ( ! empty($_POST['azienda'])) {
        update_user_meta($user_id, 'azienda', $_POST['azienda']);
    }
    if ( ! empty($_POST['indirizzo'])) {
        update_user_meta($user_id, 'indirizzo', $_POST['indirizzo']);
    }
    if ( ! empty($_POST['CAP'])) {
        update_user_meta($user_id, 'CAP', $_POST['CAP']);
    }
    if ( ! empty($_POST['citta'])) {
        update_user_meta($user_id, 'citta', $_POST['citta']);
    }
    if ( ! empty($_POST['provincia'])) {
        update_user_meta($user_id, 'provincia', $_POST['provincia']);
    }
    if ( ! empty($_POST['nazione'])) {
        update_user_meta($user_id, 'nazione', $_POST['nazione']);
    }
    if ( ! empty($_POST['professione'])) {
        update_user_meta($user_id, 'professione', $_POST['professione']);
    }
    if ( ! empty($_POST['telefono'])) {
        update_user_meta($user_id, 'telefono', $_POST['telefono']);
    }
    if ( ! empty($_POST['fax'])) {
        update_user_meta($user_id, 'fax', $_POST['fax']);
    }
    if ( ! empty($_POST['comment'])) {
        update_user_meta($user_id, 'comment', $_POST['comment']);
    }

}

add_action('user_register', 'tml_user_register');

function tml_user_update($user_id)
{
    if ( ! empty($_POST['first_name'])) {
        update_user_meta($user_id, 'first_name', $_POST['first_name']);
    }
    if ( ! empty($_POST['last_name'])) {
        update_user_meta($user_id, 'last_name', $_POST['last_name']);
    }
    if ( ! empty($_POST['azienda'])) {
        update_user_meta($user_id, 'azienda', $_POST['azienda']);
    }
    if ( ! empty($_POST['indirizzo'])) {
        update_user_meta($user_id, 'indirizzo', $_POST['indirizzo']);
    }
    if ( ! empty($_POST['CAP'])) {
        update_user_meta($user_id, 'CAP', $_POST['CAP']);
    }
    if ( ! empty($_POST['citta'])) {
        update_user_meta($user_id, 'citta', $_POST['citta']);
    }
    if ( ! empty($_POST['provincia'])) {
        update_user_meta($user_id, 'provincia', $_POST['provincia']);
    }
    if ( ! empty($_POST['nazione'])) {
        update_user_meta($user_id, 'nazione', $_POST['nazione']);
    }
    if ( ! empty($_POST['professione'])) {
        update_user_meta($user_id, 'professione', $_POST['professione']);
    }
    if ( ! empty($_POST['telefono'])) {
        update_user_meta($user_id, 'telefono', $_POST['telefono']);
    }
    if ( ! empty($_POST['fax'])) {
        update_user_meta($user_id, 'fax', $_POST['fax']);
    }

}

add_action('profile_update', 'tml_user_register');

?>
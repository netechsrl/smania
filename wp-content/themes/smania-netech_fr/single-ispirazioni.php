<?php get_header(); ?>

<style>

.prod_correlati {
    /*height: 200px;*/
	position:relative;
}




</style>

<div class="container">
<?php while ( have_posts() ) : the_post(); ?>

  <?php 

		$image1 = get_field('immagine_piccola');
		$image2 = get_field('immagine_grande');
		$size1 = 'azienda_small';
		$size2 = 'ispirazioni_big';
		$testo = get_the_content();
		$text_color = get_field('colore_testo');
		$bg_color = get_field('colore_sfondo');
	?>
  <div class="col-xs-12 blocco ispirazioni clearfix" >
    <div class="row">
      <div class="col-md-9">
        <div class="txt txt1_h" style="background-color:<?php echo $bg_color; ?>; color:<?php echo $text_color; ?>;"> <?php echo $testo;?> </div>
      </div>
      <div class="col-xs-3 desktop-visible">
        <?php 
			echo wp_get_attachment_image( $image1, $size1 );
		?>
      </div>
    </div><!-- / .row -->
    <div class="row">
      <div class="col-xs-12">
        <?php 
			echo wp_get_attachment_image( $image2, $size2 );
		?>
        
        <div>
					   <?php $images = get_field('gallery');
                        if( $images ):  
						
						?>
                        	<?php if (count($images) >1){?>
                            <div class="blocco_gallery">
                                <div class="swiper-container">
                                  <div class="swiper-wrapper">
                                    <?php foreach( $images as $image ): 
                                            $content = '<div class="swiper-slide">';
                                            //    $content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
                                                     $content .= '<img src="'. $image['sizes'][$size2] .'" alt="'. $image['alt'] .'"  />';
                                            //    $content .= '</a>';
                                            $content .= '</div>';
                                            if ( function_exists('slb_activate') ){
                                            $content = slb_activate($content);
                                            }
                                            echo $content;
                                            endforeach; ?>
                                  </div>
                                 
                                  <!--Add Pagination --> 
                            	  <div class="swiper-pagination"></div>
                                  <!--Add Navigation -->
                                  <div class="swiper-button-prev swiper-button-white"></div>
                                  <div class="swiper-button-next swiper-button-white"></div>
                                  <?php //}?>
                                </div>
                              
                            </div>
                          <?php }else{
							  	
                                echo '<img src="'. $images[0]['sizes'][$size2] .'" alt="'. $image['alt'] .'"  />';
                                }?>
                                
                      <?php 
					  endif; ?>
                    </div>
        
        
      </div>
    </div><!-- / .row -->
  </div><!-- / .blocco -->
    <?php 
	//if( have_rows('finiture_ispirazioni') ){
		//while ( have_rows('finiture_ispirazioni') ) {
		//the_row();
        $img_finiture = get_field('finiture_ispirazioni');
		if($img_finiture){ ?>
  <section id="ispiraz_finiture">
  	
    <?php 
	foreach( $img_finiture as $color ){ 
		$finitura = get_field('sel_finiture', $color->ID);
		$scheda = get_field('scheda_pdf', $finitura->ID);
		$tit = get_the_title($finitura->ID);
		$tipo = get_field('tipo_finitura', $finitura->ID);
		$titolo = $tit.' '.$tipo;
		$sottotitolo = get_the_title($color->ID);
	?>
    <div class="finiture_grid" style="max-width:136px;">
    	<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="<?php echo get_the_post_thumbnail_url( $color->ID, 'full' ); ?>">
        	<?php echo get_the_post_thumbnail( $color->ID, 'thumbnail' ); ?>
        </a>
         <div class="col-xs-9" style="padding:0 4px;">
         	<h1><?php echo $titolo ?></h1>
         	<p><?php echo $sottotitolo ?></p>
         </div>
         <div class="col-xs-3" style="padding:8px 0;">
         <?php if ($scheda){ ?>
         <a href="<?php echo $scheda ?>" target="_blank" style="padding:0;"><img style=" padding-left:10px; width:auto; float:none;" src="<?php echo get_stylesheet_directory_uri() ?>/img/icn-scheda_tecnica.png"></a>
         <?php }?>
         </div>
    </div>
    <?php 
		} // foreach( $img_finiture as $color )
	?>
  
  </section>
  <?php } //if($img_finiture) ?>
  <div style="clear:both;"></div>
  <?php 
		$prodotti_correlati = get_field('prodotti_correlati');
		if( $prodotti_correlati ){ ?>
   <h4 style="margin: 40px 20px 10px; color:#A46F25;"><?php echo strtoupper(__('Related products')) ?></h4>

</div><!-- / .container -->
<div style="clear:both; border-top:1px solid #A46F25;"></div>
<div class="container">
  <section id="prod_correlati">
  	
              <div class="prod_correlati">
                 <div class="swiper-wrapper">

	<?php		foreach( $prodotti_correlati as $prod ){  
    ?>    
              <div class="swiper-slide" style="text-align:center;">
              <a href="<?php the_permalink($prod->ID) ?>">
                  <?php echo get_the_post_thumbnail( $prod->ID, 'prodotto' ); ?>
                  <h5 style="color:#A46F25; text-transform:uppercase;"><?php echo get_the_title( $prod->ID ); ?></h5>
              </a>   
              </div>
              
              
          <?php }//foreach( $prodotti_correlati as $prod ) ?>
          
      
				</div>
            <div class="swiper-button-prev swiper-button-oro"></div>
			<div class="swiper-button-next swiper-button-oro"></div>
            </div>
  </section>
  <?php }//if( $prodotti_correlati ) ?>
</div>  <!-- / .container -->
  <?php endwhile; //while ( have_posts() ) ?>


<?php get_footer(); ?>



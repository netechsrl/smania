<?php /*
Template Name: Blog template
*/
?>
<?php get_header('page'); ?>

<?php while ( have_posts() ) : the_post(); ?>
<div class="container">

    <?php 

	// check if the flexible content field has rows of data
if( have_rows('elementi') ):
     // loop through the rows of data
    while ( have_rows('elementi') ) : the_row(); 
		//$the_link = '';
		$image1 = get_sub_field('img1');
		$size1 = 'home_vert_big';
		$size2 = 'home_orizz_big';
	?>
		<div class="col-md-6 blocco home clearfix" style="padding:10px 0 0 0;">
			<!-- modello_1 -->
			<?php if( get_row_layout() == 'modello_1' ): ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
            <div class="col-xs-6">
              <div class="txt_container">
                <div class="txt txt2_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo');?>;">
                  <h1><?php the_title();?></h1>
                  <?php the_excerpt();?>
                  </div>
              </div>
            </div>
			<div class="col-xs-6">
            	<div class="img_big_vertical">
                <?php 
                $image1 = get_sub_field('img1');
                echo wp_get_attachment_image( $image1, $size1 );
				?>
                </div>
			</div>
			</a>
			<!-- modello_2 -->
			<?php elseif( get_row_layout() == 'modello_2' ): ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
			<div class="col-xs-12 clearfix" style="margin-top: 10px;">
              <div class="txt_container">
                <div class="txt txt3_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo');?>;">
                  <h1><?php the_title();?></h1>
                  <?php the_excerpt();?>
                  </div>
              </div>
            </div>
			<div class="col-xs-12">
            	<div class="img_big_orizzontal">
                <?php 
                $image1 = get_sub_field('img1');
                echo wp_get_attachment_image( $image1, $size2 );
				?>
                </div>
			</div>
			</a>
           <?php endif; // get_row_layout() ==?>
          </div><!-- .blocco -->

		
    <?php  endwhile; ?>
	<?php endif;
	endwhile;
	?>
</div>
<!-- #container -->

<?php get_footer(); ?>

<?php get_header("page");

if (current_user_can('read')) { ?>

    <div id="primary" class="container area-ris">
        <div id="content" role="main">
        	<div class="welcome_txt">
            	<?php echo get_field('testo2', 'option' ) ?>
            </div>


            <div class="clearfix">
                <?php
                    ?>
                    <div class="panel-body" >
                        <?php
							$files = get_field('elenco_file');
							foreach ($files as $file) {
								?>
								<div class="col-sm-9" style="padding: 30px 0 0 20px;">
									<span class="col-xs-2" style="margin-right:40px;"><img style="max-width: 80px;" src="<?php echo $file['immagine']; ?>"></span>
									<span class="col-xs-2" style="font-weight:bold; margin-right:40px;"><?php echo $file['nome_allegato']; ?></span>
									<span class="col-xs-2" style="margin-right:40px;"><?php echo $file['data_allegato']; ?></span>
									<span class="col-xs-2" style="margin-right:40px;"><?php echo $file['info_allegato']; ?></span>
								</div>
                                <div class="col-sm-3" style="padding: 30px 0 0 20px;">
                                	<a class="download-btn" href="<?php echo $file['allegato']; ?>"><?php echo __('Download') ?></a>
                                </div>
								<div style="clear:both;"></div>
							<?php } ?>
						
                    </div>
            </div>

        </div>   <!-- #content -->
    </div> <!-- #primary -->
<?php } else {
    echo "<script language='javascript'>\n";
    echo "  location.href=\"".home_url('/login')."\"";
    echo "</script>\n";
}

get_footer(); ?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php wp_title('&raquo;', 'true', 'right'); ?>
    </title>
    <meta name="description" content="<?php //echo get_option('my_description'); ?>"/>

    <!-- Favicon -->
    <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/lib/yamm/yamm.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="<?php echo get_stylesheet_directory_uri(); ?>/lib/nivo-lightbox/dist/nivo-lightbox.min.css">
    <link rel="stylesheet"
          href="<?php echo get_stylesheet_directory_uri(); ?>/lib/nivo-lightbox/themes/default/default.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lib/Swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/js/slicknav/dist/slicknav.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/custom.style.css" rel="stylesheet">
    <!--    <link href="<?php echo get_stylesheet_directory_uri(); ?>/custom.style.css" rel="stylesheet">-->

    <!-- Google web Font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,100' rel='stylesheet' type='text/css'>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1286086064837921'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1286086064837921&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- search button -->
            <div class="navbar-header nav-search">
                <button type="button" class="navbar-search_btn  dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true" id="search-form">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="search-form" style="opacity:.7;">
                    <form>
                        <input type="text" name="search" id="s" value="" placeholder="<?php echo __('Search...'); ?>"
                               class="ui-autocomplete-input" autocomplete="off"
                               style="font-size: 30px; text-align: center;">
                    </form>
                </div>
            </div>
            <!-- search button -->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="mobile-view">
                <div class="navbar-header">
                    <div style="position: absolute; top: 0; left: 0;">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#menu-mobile" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div>
                        <?php $logo = get_field('logo', 'option'); ?>
                        <a class="navbar-brand"
                           href="<?php echo home_url('/'); ?>"><?php echo wp_get_attachment_image($logo, 'logo'); ?></a>
                    </div>
                </div>

                <!-- menu mobile -->
                <div class="collapse navbar-collapse" id="menu-mobile">
                    <?php
                    wp_nav_menu(array(
                        'menu'           => 'mobile',
                        'theme_location' => 'mobile',
                        'depth'          => 4,
                        //                        'container'       => 'div',
                        //                        'container_class' => 'collapse navbar-collapse',
                        'container_id'   => 'bs-example-navbar-collapse-3',
                        'menu_class'     => 'nav navbar-nav',
                        'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
                        'walker'         => new wp_bootstrap_navwalker(),
                    ));
                    ?>
                </div><!-- /.navbar-collapse mobile -->
            </div> <!-- / .mobile-view-->

            <div class="desktop-view">
                <div class="navbar-header">
                    <!--                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"-->
                    <!--                            data-target="#menu-secondary" aria-expanded="false">-->
                    <!--                        <span class="sr-only">Toggle navigation</span>-->
                    <!--                        <span class="icon-bar"></span>-->
                    <!--                        <span class="icon-bar"></span>-->
                    <!--                        <span class="icon-bar"></span>-->
                    <!--                    </button>-->
                    <!-- menu secondary -->
                    <div id="menu-secondary">
                        <?php
                        wp_nav_menu(array(
                            'menu'            => 'secondary',
                            'theme_location'  => 'secondary',
                            'depth'           => 4,
                            'container'       => 'div',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id'    => 'bs-example-navbar-collapse-2',
                            'menu_class'      => 'nav navbar-nav yamm navbar-right',
                            'fallback_cb'     => 'Yamm_Nav_Walker_menu_fallback',
                            'walker'          => new Yamm_Fw_Nav_Walker(),
                        ));
                        ?>
                    </div><!-- /.navbar-collapse secondary -->
                    <?php $logo = get_field('logo', 'option'); ?>
                    <a class="navbar-brand"
                       href="<?php echo home_url('/'); ?>"><?php echo wp_get_attachment_image($logo, 'logo'); ?></a>
                </div>

                <div class="desktop-main-menu">
                    <?php
                    wp_nav_menu(array(
                        'menu'            => 'primary',
                        'theme_location'  => 'primary',
                        'depth'           => 4,
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id'    => 'bs-example-navbar-collapse-1',
                        'menu_class'      => 'nav navbar-nav yamm navbar-right',
                        'fallback_cb'     => 'Yamm_Nav_Walker_menu_fallback',
                        'walker'          => new Yamm_Fw_Nav_Walker(),
                    ));
                    ?>
                </div>
            </div> <!-- / .desktop-view-->


        </div><!-- /.container-fluid -->
    </nav>


    <!-- Swiper -->

    <?php
    $page_id = get_the_id();
    global $post;
    $content = $post->post_content;

    $args   = array(
        'post_type'      => 'cover-slider',
        //        'orderby' => 'date',
        //        'order' => 'DESC',
        'post_status'    => 'publish',
        'meta_key'       => 'page_slider',
        'meta_value'     => $page_id,
        'posts_per_page' => 1,
    );
    $slider = new WP_Query($args);
    if ($slider->have_posts()) {

        ?>
        <?php while ($slider->have_posts()) : $slider->the_post();
            if (have_rows('immagini_slide', $slider->ID)):
                $images = get_field('immagini_slide');
                if (count($images) > 1) {
                    ?>
                    <div class="swiper-container">
                        <div class="swiper-wrapper">

                            <?php // loop through rows (categoria_prodotto)
                            while (have_rows('immagini_slide', $slider->ID)): the_row();
                                $image = get_sub_field('img_slide');
                                //var_dump($image);
                                ?>
                                <div class="swiper-slide" style="background-image:url(<?php echo $image; ?>);">
                                    <div class="swiper-filter"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="box-carousel">
                                                    <?php if (get_sub_field('dida_slide')) { ?>
                                                        <h1><?php echo get_sub_field('dida_slide') ?></h1>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; // while( have_rows('immagini_slide') ):

                            wp_reset_postdata();
                            ?>
                        </div>


                        <!--Add Pagination -->
                        <div class="container" style="position:relative;">
                            <div class="swiper-pagination"></div>
                        </div>
                        <!--Add Navigation -->
                        <div class="container" style="position:relative;height: 100%;margin-top: -560px;">
                            <div class="swiper-button-prev swiper-button-white"></div>
                            <div class="swiper-button-next swiper-button-white"></div>
                        </div>
                    </div><!-- /Swiper -->
                    <?php
                } else {

                    while (have_rows('immagini_slide', $slider->ID)): the_row();
                        $image = get_sub_field('img_slide'); ?>

                        <div class="swiper-slide one_image" style="background-image:url(<?php echo $image; ?>); margin-bottom:0;">
                            <div class="swiper-filter"></div>
                            <div class="container">
                                <div class="row">
                                    <div class="box-carousel">
                                        <div class="<?php if ( $content && (!is_singular() || !is_single()) ){ ?>col-sm-3<?php }else{ ?>col-sm-12<?php } ?>">
                                            <?php if (get_sub_field('dida_slide')) { ?>
                                                <h1><?php echo get_sub_field('dida_slide') ?></h1>
                                            <?php } else { ?>
                                                <h1><?php echo get_the_title($page_id); ?></h1>
                                            <?php } ?>
                                        </div>
                                        <?php if ($content && (!is_singular() || !is_single())) { ?>
                                            <div class="col-sm-9">
                                                <p><?php echo $content; ?></p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile;
                    wp_reset_postdata();
                }// if (count($images) >1)

            endif; // if( have_rows('immagini_slide') ): ?>
            <?php
        endwhile;

    } // if ($slider->have_posts())
    ?>
    <!-- / Testata slider o immagine -->

    <?php /***** Breadcrumbs *****/
    if ( ! is_front_page()) {

        if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('
<nav class="breadcrumb" id="breadcrumbs"><div class="container" style="padding-left:40px;">', '</div></nav>
');
        }

    } // !is_front_page()
    /***** end Breadcrumbs *****/ ?>


</header>




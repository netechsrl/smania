<?php get_header('page'); ?>

<div class="container">
  <div id="primary">
    <div id="content" class="col-sm-9 " role="main">
      <?php $i = 0; ?>
      <?php while ( have_posts() ) : the_post(); 
  
  ?>
      
      <div class="row" style="padding:20px 0; border-bottom:1px solid #a56a4c; ">
       
        <a href="<?php the_permalink() ?>">
        <div class="col-xs-3 item-grid">
          <?php the_post_thumbnail('prodotto') ?>
        </div>
        <div class="col-xs-9">
          <p class="grid-tit" style="text-align:left;">
            <?php the_title();?>
          </p>
          <p>
          <?php the_content();?>
          </p>
        </div>
        </a>
       
      </div>
      <div class="clearfix"></div>
      
      <?php endwhile; // end of the loop. ?>
    </div>
  </div>
  <!-- #content -->
  <div class="col-sm-3">
    <?php  get_sidebar('prodotti'); ?>
  </div>
</div>
<!-- #primary -->
</div>
<!-- / .container -->
<?php get_footer(); ?>

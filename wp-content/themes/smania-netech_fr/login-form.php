<?php if (isset($_GET["registration"]) && $_GET["registration"] == "complete") { ?>
    <div class="container" style="margin-top: 50px; margin-bottom: 50px;">
        <div class="success-msg">
            Gentile cliente,<br>
            grazie di esserti registrato!<br>
            Inserisci le tue credenziali ed entra nell'area riservata.<br><br>
        </div>
    </div>
<?php } else { ?>
    <div class="col-md-6">
        <div class="registration-msg">
            <span class="title-msg">Se non sei ancora iscritto,</span>
            <div class="button-pnl">
                <a href="/register"><div class="btn btn-default">Registrati</div></a>
            </div>
            <div class="msg">
                <span class="title-msg">Sei un architetto, un rivenditore o un giornalista?<br></span>
                <span>Regitrati per accedere all'area riservata dove potrai trovare cataloghi, file 2D, file 3D, cartelle stampa e altri documenti utili.</span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="tml tml-login" id="theme-my-login<?php $template->the_instance(); ?>">
            <span class="title-msg">Inserisci la tua Email e la tua Password di accesso</span>
            <?php //$template->the_action_template_message('login'); ?>
            <?php //$template->the_errors(); ?>
            <form name="loginform" id="loginform<?php $template->the_instance(); ?>"
                  action="<?php $template->the_action_url('login', 'login_post'); ?>" method="post">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="log" id="user_login<?php $template->the_instance(); ?>" class="input"
                           value="<?php $template->the_posted_value('log'); ?>" size="20" placeholder="EMAIL"/>
                </div>
                </p>

                <p class="tml-user-pass-wrap">
                <div class="styled-select">
                    <input type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" class="input"
                           value="" size="20" autocomplete="off" placeholder="PASSWORD"/>
                </div>
                </p>

                <?php do_action('login_form'); ?>

                <div class="tml-rememberme-submit-wrap">
                    <p class="tml-submit-wrap">
                        <input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>"
                               value="<?php esc_attr_e('Log In', 'theme-my-login'); ?>"/>
                        <input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url('login'); ?>"/>
                        <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>"/>
                        <input type="hidden" name="action" value="login"/>
                    </p>
                </div>
            </form>
            <span class="recover-msg">Hai dimenticato la password? <strong>Attiva ora</strong><?php //$template->the_action_links(array('login' => false)); ?>
                la procedura di recupero Password</span>
        </div>
    </div>
<?php } ?>


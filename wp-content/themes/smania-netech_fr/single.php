<?php get_header('page'); ?>
<div class="container">
	<div class="col-md-9">
<?php while ( have_posts() ) : the_post(); ?>

  <?php 
  	if( have_rows('elementi') ):
		while ( have_rows('elementi') ) : the_row(); 
			$image1 = get_sub_field('img1');
		endwhile;
	endif;
		$size = 'ispirazioni_big';
		$testo = get_the_content();
	?>
  <div class="col-xs-12 blocco ispirazioni clearfix" >
    
    <div class="row">
      <div class="col-xs-12" style="max-height:620px; overflow:hidden;">
					   <?php $images = get_field('gallery');
                        if( $images ):  
						
						?>
                        	<?php if (count($images) >1){?>
                            <div class="blocco_gallery">
                                <div class="swiper-container">
                                  <div class="swiper-wrapper">
                                    <?php foreach( $images as $image ): 
                                            $content = '<div class="swiper-slide">';
                                                //$content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
                                                     $content .= '<img src="'. $image['sizes'][$size] .'" alt="'. $image['alt'] .'"  />';
                                                //$content .= '</a>';
                                            $content .= '</div>';
                                            if ( function_exists('slb_activate') ){
                                            $content = slb_activate($content);
                                            }
                                            echo $content;
                                            endforeach; ?>
                                  </div>
                                 
                                  <!--Add Pagination --> 
                            		<div class="swiper-pagination"></div>
                                  <!--Add Navigation -->
                                  <div class="swiper-button-prev swiper-button-white"></div>
                                  <div class="swiper-button-next swiper-button-white"></div>
                                  <?php //}?>
                                </div>
                              
                            </div>
                          <?php }else{
							  	
                                echo '<img src="'. $images[0]['sizes'][$size] .'" alt="'. $image['alt'] .'"  />';
                                }?>
                                
                      <?php 
					  else:
					  
						echo wp_get_attachment_image( $image1, $size );
						
					  endif; ?>
                   
        
        
      </div>
    </div><!-- / .row (img)-->
    <div class="row">
    
    <h1 style="color:#a46e24;"><?php the_title()?></h1>
    <?php the_date('d.m.Y', '<div style="padding-bottom:20px;">', '</div>'); ?>
   <?php  the_content() ?>
   
   <?php 
   // Previous/next post navigation.
			my_the_post_navigation( array(
				'next_text' => '<div style="float:right; color:#a46e24;"><span class="post-title">%title</span>'.
					'<span class="meta-nav" aria-hidden="true"> > </span> </div>',
				'prev_text' => '<div style="float:left; color:#a46e24;"><span class="meta-nav" aria-hidden="true"> < </span> ' .
					'<span class="post-title">%title</span></div>',
			) );
   
   ?>
   
    
    </div><!-- / .row (txt)-->
    <div class="row">
    	<div class="col-md-6 single_share">
        	<ul class="blog-list share">
                        <li>
                        <h1><?php echo __('share') ?></h1>
                        </li>
                        <span>
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" rel="nofollow" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/home?status=<?php the_permalink() ?>" rel="nofollow" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" rel="nofollow" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </span>
        </ul>
        </div>
        <a href="<?php echo home_url( '/'.__('products') ); ?>">
        <div class="col-md-6 single_btn_prod">
        	<ul class="blog-list" >
                        <li>
                        <h1><?php echo __('discover our products') ?></h1>
                        </li>
            </ul>
        </div>
        </a>
     
    </div><!-- / .row (btns)-->
    
  </div><!-- / .blocco -->
  <?php endwhile; ?>
  
  
	</div>
    <div class="col-md-3">
    <?php get_sidebar(); ?>
    </div>  
</div><!-- / .container -->


<?php get_footer(); ?>

<div class="desktop-view" style="background-image:url(<?php echo get_field('img_header',
    'option'); ?>); background-size:cover; background-position:center center; height:132px;margin:10px 0;">
    <div class="container">
        <div class="col-sm-6" style="text-align:center;">
            <h1 style="color:#a46e24;">newsletter</h1>
            <p><?php echo __('Inserisci il tuo indirizzo e-mail e iscriviti alla nostra newsletter<br>per essere sempre aggiornato sulle novità e gli eventi Smania') ?></p>
        </div>
        <div class="col-sm-6">
            <!-- Begin MailChimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">

            <div id="mc_embed_signup_smania">
                <form action="//smania.us15.list-manage.com/subscribe/post?u=c2ac056482d214c9a3c819ec7&amp;id=b5f9c2a6d5"
                      method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
                      target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">

                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL"
                               placeholder="example@email.com" required>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                                  name="b_c2ac056482d214c9a3c819ec7_b5f9c2a6d5"
                                                                                                  tabindex="-1"
                                                                                                  value=""></div>
                        <div class="clear"><input type="submit" value=" &rang;" name="subscribe"
                                                  id="mc-embedded-subscribe"
                                                  class="button swiper-button-next swiper-button-oro"></div>
                    </div>
                </form>
            </div>

            <!--End mc_embed_signup-->


        </div>
    </div>


</div>
<div class="container">
    <div class="col-md-12 footer desktop-view">

        <div class="col-sm-5" style="padding-left:0;">
            <div style="padding-left:0;">
                <div><?php echo wp_get_attachment_image(get_field('logo', 'option'), 'logo'); ?></div>
                <div style="padding: 50px 0;">
                    <img style="width:auto; float:left;"
                         src="<?php echo get_stylesheet_directory_uri(); ?>/img/icn-location.png">
                    <div style="padding-left:30px;">
                        <?php echo get_field('footer_address', 'option') ?>

                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-7">
            <div class="col-sm-3">
                <?php /* Primary navigation */
                wp_nav_menu(array(
                    'menu'           => 'footer',
                    'theme_location' => 'footer',
                    'depth'          => 1,
                    'container'      => false,
                    'menu_class'     => 'nav navbar-nav',
                    //Process nav menu using our custom nav walker
                    'walker'         => new wp_bootstrap_navwalker(),
                ));
                ?>

            </div>
            <div class="col-sm-3">
                <ul>
                    <li style="padding: 10px 0 5px;"><a href="#">privacy policy</a></li>
                    <li style="padding: 10px 0 5px;"><a href="#">faq</a></li>
                    <li style="padding: 10px 0 5px;"><a href="#">press</a></li>
                    <li style="padding: 10px 0 5px;"><a href="#">cataloghi</a></li>
                </ul>
            </div>
            <div class="col-sm-6 social">
                <ul style="text-align:center;">
                    <li><a href="<?php echo get_field('facebook', 'option'); ?>" target="_blank"><i
                                    class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo get_field('twitter', 'option'); ?>" target="_blank"><i
                                    class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo get_field('google+', 'option'); ?>" target="_blank"><i
                                    class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo get_field('pinterest', 'option'); ?>" target="_blank"><i
                                    class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo get_field('instagram', 'option'); ?>" target="_blank"><i
                                    class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo get_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-youtube"
                                                                                                   aria-hidden="true"></i></a>
                    </li>
                    <?php if (have_rows('aggiungi_pagina_social_link', 'option')):
                        $socials = get_field('aggiungi_pagina_social_link', 'option');
                        while (have_rows('aggiungi_pagina_social_link', 'option')): the_row();
                            $social = get_sub_field('img_slide'); ?>
                            <li><a href="<?php echo get_sub_field('url', 'option'); ?>"
                                   target="_blank"><?php echo get_sub_field('icona', 'option'); ?></a></li>

                        <?php endwhile; ?>

                    <?php endif; //(have_rows('aggiungi_pagina_social_link') ?>
                </ul>
                <div style="background-color:#a46e24; color:#fff; text-align:center; padding:2px 0; margin-top: 10px;">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo get_field('email',
                        'option'); ?></div>
                <div style="text-align:center; padding:6px 0;"><i class="fa fa-phone" aria-hidden="true"></i></i>&nbsp;&nbsp;&nbsp;<?php echo get_field('telefono',
                        'option'); ?></div>
                <div style="text-align:center; padding:6px 0; margin-top: 50px;"><?php echo get_field('copyright',
                        'option'); ?></div>
                <div style="text-align:center; padding:6px 0; border-bottom:1px solid #a46e24; border-top:1px solid #a46e24;">
                    <a href="<?php echo home_url() ?>/credits">credits</a></div>

            </div>
        </div>


    </div>

    <div class="col-md-12 footer mobile-view">

        <div class="social" style="text-align:center;">
            <ul>
                <li><a href="<?php echo get_field('facebook', 'option'); ?>" target="_blank"><i
                                class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo get_field('twitter', 'option'); ?> target=" _blank""><i
                            class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo get_field('google+', 'option'); ?>" target="_blank"><i
                                class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo get_field('pinterest', 'option'); ?>" target="_blank"><i
                                class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo get_field('instagram', 'option'); ?>" target="_blank"><i
                                class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo get_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-youtube"
                                                                                               aria-hidden="true"></i></a>
                </li>
                <?php if (have_rows('aggiungi_pagina_social_link', 'option')):
                    $socials = get_field('aggiungi_pagina_social_link', 'option');
                    while (have_rows('aggiungi_pagina_social_link', 'option')): the_row();
                        $social = get_sub_field('img_slide'); ?>
                        <li><a href="<?php echo get_sub_field('url', 'option'); ?>"
                               target="_blank"><?php echo get_sub_field('icona', 'option'); ?></a></li>

                    <?php endwhile; ?>

                <?php endif; //(have_rows('aggiungi_pagina_social_link') ?>
            </ul>
            <div style="background-color:#a46e24; color:#fff; text-align:center; padding:2px 0; margin-top: 10px;">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php echo get_field('email',
                    'option'); ?></div>
            <div style="text-align:center; padding:6px 0;"><i class="fa fa-phone" aria-hidden="true"></i></i>&nbsp;&nbsp;&nbsp;<?php echo get_field('telefono',
                    'option'); ?></div>
            <div style="text-align:center; padding:6px 0; margin-top: 10px;"><?php echo get_field('copyright',
                    'option'); ?></div>
            <div style="text-align:center; padding:6px 0; border-bottom:1px solid #a46e24; border-top:1px solid #a46e24;">
                <a href="<?php echo home_url() ?>/credits">credits</a></div>

        </div>

    </div>


</div><!--  / .container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://code.jquery.com/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/js/modernizr-2.8.3.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/isotope.pkgd.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lib/nivo-lightbox/dist/nivo-lightbox.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lib/Swiper/dist/js/swiper.min.js"></script>

<script>

    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        speed: 1000,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        autoplay: 4500,
        loop: true,
    });
    var swiper_prod = new Swiper('.swiper-container-prod', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        speed: 1000,
        nextButton: '.swiper-button-next-prod',
        prevButton: '.swiper-button-prev-prod',
        autoplay: 4500,
        loop: true,
    });
    var swiper_storia = new Swiper('.swiper-container-storia', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        speed: 1000,
        nextButton: '.swiper-button-next-storia',
        prevButton: '.swiper-button-prev-storia',
        autoplay: 4500,
        loop: true,
    });
    var swiper1 = new Swiper('.prod_correlati', {
        //pagination: '.swiper-pagination',
        //paginationClickable: true,
        slidesPerView: 4,
        spaceBetween: 50,
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40
            },
            769: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        },
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });


</script>


<script>
    $(document).ready(function () {
        $('.nivo').nivoLightbox();
    });
</script>

<script>
    $(function () {

        $('#slide-submenu').on('click', function () {
            $(this).closest('.list-group').fadeOut('slide', function () {
                $('.mini-submenu').fadeIn();
            });

        });

        $('#search-form').on('click', function () {
            $(this).next('.dropdown-menu').toggle('slide');
            //$('#search-form').hide();
        })
    })
</script>
<script>
    $(document).on('click', '.yamm .dropdown-menu', function (e) {
        e.stopPropagation()
    })

    // Add slideDown animation to Bootstrap dropdown when expanding.
    $('.dropdown').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().toggle('slide');
    });

    $('.dropdown').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().toggle('slide');
    });
</script>
<script type="text/javascript">
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 50) {
            $('#return-to-top').fadeIn(200);
        } else {
            $('#return-to-top').fadeOut(200);
        }
    });
    $('#return-to-top').click(function () {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, 500);
        return false;
    });

    $('input[name=user_email]').keyup(function (e) {
        $('input[name=user_login]').val($('input[name=user_email]').val());
    });

    $("form#your-profile select[name=provincia] option").each(function (index) {
        if ($(this).val() == provincia) {
            $(this).attr("selected", "selected");
        }
    });
    $("form#your-profile select[name=nazione] option").each(function (index) {
        if ($(this).val() == nazione) {
            $(this).attr("selected", "selected");
        }
    });
    $("form#your-profile select[name=professione] option").each(function (index) {
        if ($(this).val() == professione) {
            $(this).attr("selected", "selected");
        }
    });
</script>


<?php wp_footer(); ?>
<a href="#" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
</body>
</html>

    	
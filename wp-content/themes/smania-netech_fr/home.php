<?php /*

*/
?>

<?php get_header('page'); ?>

<div class="container">
    <div style="padding-top:30px; padding-left:5px;">
        <div class="col-sm-3">
            <ul class="blog-list">
                <li>
                    <span style="text-transform:uppercase; color: #a56a4c;"><?php echo __('Share') ?></span>
                </li>
                <span>
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>"
                               rel="nofollow" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/home?status=<?php the_permalink() ?>" rel="nofollow"
                               target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" rel="nofollow"
                               target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </span>
            </ul>
        </div>
        <div class="col-sm-9">
            <ul id="filters" class="blog-list blog-category">
                <?php
                $terms = get_terms('category');
                $count = count($terms);
                echo '<li><a href="javascript:void(0)" title="" data-filter=".all" class="active">'.__('All').'</a></li>';
                if ($count > 0) {

                    foreach ($terms as $term) {

                        if ($term->term_id != 1) {
                            $termname = strtolower($term->name);
                            $termname = str_replace(' ', '-', $termname);
                            echo '<li><a href="javascript:void(0)" title="" data-filter=".'.$termname.'">'.$term->name.'</a></li>';
                        }
                    }
                }
                ?>
            </ul>
        </div>
    </div>

    <div id="blog-posts">
        <?php while (have_posts()) : the_post();

            // check if the flexible content field has rows of data
            if (have_rows('elementi')):
                // loop through the rows of data
                while (have_rows('elementi')) : the_row();

                    $terms = get_the_terms($post->ID, 'category');
                    if ($terms && ! is_wp_error($terms)) :

                        $links = array();

                        foreach ($terms as $term) {
                            $links[] = $term->name;
                        }

                        $tax_links = join(" ", str_replace(' ', '-', $links));
                        $tax       = strtolower($tax_links);
                    else :
                        $tax = '';
                    endif;

                    //$the_link = '';
                    $image1 = get_sub_field('img1');
                    $size1  = 'home_vert_big';
                    $size2  = 'home_orizz_big';
                    //var_dump($image1);
                    ?>
                    <div class="col-md-6 blocco home clearfix all portfolio-item  <?php echo $tax ?>"
                         style="padding:10px 0 0 0;">
                        <!-- modello_1 -->
                        <?php if (get_row_layout() == 'modello_1'): ?>
                            <a href="<?php the_permalink() ?>">
                                <div class="col-xs-6">
                                    <div class="txt_container">
                                        <div class="txt txt2_h"
                                             style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo'); ?>;">
                                            <h1><?php the_title(); ?></h1>
                                            <?php the_excerpt(); ?>
                                            <div style="padding-top:10px; text-transform:uppercase;"><?php echo __('read more').' >>'; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="img_big_vertical">
                                        <?php
                                        echo wp_get_attachment_image($image1, $size1);
                                        ?>
                                    </div>
                                </div>
                            </a>
                            <!-- modello_2 -->
                        <?php elseif (get_row_layout() == 'modello_2'): ?>
                            <a href="<?php the_permalink() ?>">
                                <div class="col-xs-12 clearfix">
                                    <div class="txt_container">
                                        <div class="txt txt3_h"
                                             style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo'); ?>;">
                                            <h1><?php the_title(); ?></h1>
                                            <?php the_excerpt(); ?>
                                            <div style="padding-top:10px; text-transform:uppercase;"><?php echo __('read more').' >>'; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12" style="margin-top: 10px;">
                                    <div class="img_big_orizzontal">
                                        <?php
                                        echo wp_get_attachment_image($image1, $size2);
                                        ?>
                                    </div>
                                </div>
                            </a>
                        <?php endif; // get_row_layout() ==?>
                    </div><!-- .blocco -->


                <?php endwhile; ?>
            <?php endif;
        endwhile;
        ?>
    </div>
</div>
<!-- .container -->

<?php get_footer(); ?>
<script type="text/javascript">
    (function ($) {

        var $container = $('#blog-posts');

        // create a clone that will be used for measuring container width
        $containerProxy = $container.clone().empty().css({visibility: 'hidden'});

        $container.after($containerProxy);

        // get the first item to use for measuring columnWidth
        var $item = $container.find('.portfolio-item').eq(0);

        $container.imagesLoaded(function () {
            $(window).smartresize(function () {

                // calculate columnWidth
                var colWidth = Math.floor($containerProxy.width() / 2); // Change this number to your desired amount of columns

                // set width of container based on columnWidth
                $container.css({
                    width: colWidth * 2 // Change this number to your desired amount of columns
                })
                    .isotope({

                        // disable automatic resizing when window is resized
                        resizable: false,

                        // set columnWidth option for masonry
                        masonry: {
                            columnWidth: colWidth
                        }
                    });

                // trigger smartresize for first time
            }).smartresize();
        });

        // filter items when filter link is clicked
        $('#filters a').click(function () {

            var selector = $(this).attr('data-filter');
            $container.isotope({filter: selector, animationEngine: "css"});
            $('#filters a.active').removeClass('active');
            $(this).addClass('active');
            return false;

        });

    })(jQuery);
</script>

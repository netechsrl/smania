<?php /*
Template Name: Archivio ordinabile (pagine/articoli)
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) :?>
<?php while(have_posts()) : the_post(); ?>

<!--<div id="tax-description" class="col-md-8 col-md-offset-4" style="padding: 0 0 60px 23px;">
  <h1>
    <?php the_title(); ?>
  </h1>
  <?php the_content(); ?>
</div>-->

<div class="clearfix"></div>
<div class="cont-grid" >
  <div class="grid"> 
    <!--<div class="row">	-->
    <?php   
			
			$posts = get_field('item_ord');
			if( $posts ):
			//var_dump($posts);
			foreach( $posts as $p ):
			
			?>
    <div class="item">
      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="cat_prod_cover clearfix" style="padding:0;">
        <a href="<?php echo get_permalink( $p->ID ); ?>">
          <div > <?php echo get_the_post_thumbnail($p->ID, 'masonry-grid', array('class' => 'grid-cop')); ?> 
            <div class="grid-texts">
              <p class="grid-cat">
                <?php echo get_field('sottotitolo', $p->ID); ?>
              </p>
              <h2 class="grid-tit"><?php echo $p->post_title ; ?></h2>
              
              <!--<p class="meta"> <i class="fa fa-clock-o"></i> <?php the_time('j M , Y') ?> &nbsp;
                            
                                <?php 
                                $video = get_post_meta($p->ID, 'fullby_video', true );
                                
                                if($video != '') { ?>
                                        
                                    <i class="fa fa-video-camera"></i> Video
                                        
                                <?php } else if (strpos($p->post_content,'[gallery') !== false) { ?>
                                        
                                    <i class="fa fa-th"></i> Gallery
                            
                                    <?php } else {?>
                            
                                    <?php } ?>
                                    
                            </p>-->
              
              <div class="grid-text">
                <?php echo get_field('abstract', $p->ID);?>
              </div>
              <div class="myLink"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/freccia01.png" /> <span>entra</span> </div>
            </div>
          </div>
          </a> </div>
      </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    
    <!--</div>--> 
  </div>
</div>
<?php endwhile; // have_posts()) :?>
<?php else : ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; // have_posts()) :?>
<?php get_footer(); ?>

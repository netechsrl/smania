<?php /*
Template Name: Pagine Storia
*/
?>


<?php get_header(); ?>

<style>

    .swiper-slide {
        text-align: center;
        font-size: 18px;
        
        width: 50%;
		overflow:hidden;
        /* Center slide text vertically 
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;*/
    }
    /*.swiper-slide:nth-child(2n) {
        width: 60%;
    }
    .swiper-slide:nth-child(3n) {
        width: 40%;
    }*/
	
    
    </style>


	<div id="primary">
  <div id="content" role="main">
    <?php while ( have_posts() ) : the_post(); ?>
    <div class="cat_prod_cover clearfix" style="border:none;">
      <?php	$cover_img = get_field('cover_img_stories');
				if( $cover_img ):?>
      <div class="cat_cover_cat" style="max-width:598px">
        <?php  echo wp_get_attachment_image( $cover_img, 'prodotto-big' );?>
        <?php endif; ?>
        <h1>
          <?php the_title(); ?>
        </h1>
        <?php the_content(); ?>
      </div>
    </div>
    <div id="prod_galleries">
      <?php // check for rows (categoria_prodotto)
if( have_rows('gallerie_immagini') ): ?>
      <?php 

	  // loop through rows (categoria_prodotto)
	  while( have_rows('gallerie_immagini') ): the_row(); 
	 
			  $size = 'carousel-h234';
			  $width = 598;
		   ?>
      <?php $images = get_sub_field('galleria_immagine_tipo_1');
	if( $images ):  ?>
      <div class="prod_gallery clearfix">
        <div class="cat_cover_cat" style="max-width:<?php echo $width ?>px">
          <div class="swiper-container-stories" >
            <div class="swiper-wrapper">
              <?php foreach( $images as $image ): 
            $content = '<div class="swiper-slide">';
                $content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
                     $content .= '<img src="'. $image['sizes'][$size] .'" alt="'. $image['alt'] .'" />';
                $content .= '</a>';
            $content .= '</div>';
            if ( function_exists('slb_activate') ){
			$content = slb_activate($content);
			}
    
			echo $content;?>
              <?php endforeach; ?>
            </div>
            <?php //if (count($images) >3){?>
            
            <div class="swiper-button-prev-stories swiper-button-white"></div>
            <div class="swiper-button-next-stories swiper-button-white"></div>
            <?php //}?>
          </div>
          <div class="gallery_txt"><?php echo get_sub_field('testo_descrittivo');?></div>
        </div>
      </div>
      <?php endif; ?>
      <?php endwhile; // while( have_rows('gallerie_immagini') ): ?>
      <?php endif; // if( have_rows('gallerie_immagini') ): ?>
    </div>
    <?php // check for rows (categoria_prodotto)
	if( have_rows('galleria_immagini_singola') ): 
	echo '<div style="border-bottom:1px solid #000;"></div>';
	  // loop through rows (categoria_prodotto)
	  while( have_rows('galleria_immagini_singola') ): the_row(); 
	  $image = get_sub_field('galleria_immagine_tipo_2');
	  $size = 'prodotto-big';
	  $width = 598;?>
    <div class="prod_gallery_single clearfix">
      <div class="cat_cover_cat" style="max-width:<?php echo $width ?>px">
        <div >
          <?php  echo wp_get_attachment_image( $image, $size );?>
        </div>
      </div>
    </div>
    <?php endwhile; // while( have_rows('galleria_immagini_singola') ): ?>
    <?php endif; // if( have_rows('galleria_immagini_singola') ): ?>
    <?php endwhile; // end of the loop. ?>
    
    
    	
    
    
  </div>
  <!-- #content --> 
</div>
<!-- #primary -->

<?php get_footer(); ?>

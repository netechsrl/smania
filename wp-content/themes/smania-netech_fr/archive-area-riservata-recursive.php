<?php get_header("page");

if (current_user_can('read')) { ?>

    <div id="primary" class="container">
        <div id="content" role="main">

            <?php
            $myterms = get_terms(array(
                'taxonomy'   => 'cat-ar',
                'parent'     => 0,
                'orderby'    => id,
                'hide_empty' => false,
                'order'      => 'asc',
            ));
            ?>
            <ul class="nav nav-pills">
                <?php
                $active = "active";
                foreach ($myterms as $myterm) {
                    ?>
                    <li class="<?php echo $active; ?>">
                        <a href="#<?php echo $myterm->slug ?>" data-toggle="tab"><?php echo $myterm->name ?></a>
                    </li>
                    <?php
                    $active = "";
                }
                ?>
            </ul>
            <div class="tab-content clearfix">
                <?php
                $active = "active";
                foreach ($myterms as $myterm) {
                    ?>
                    <div class="tab-pane <?php echo $active; ?>" id="<?php echo $myterm->slug ?>">
                        <?php
                        // prendo i post della tassonomia corrente
                        $curr_taxonomy_posts = array();
                        while (have_posts()) {
                            the_post();
                            $product_terms = wp_get_object_terms($post->ID, 'cat-ar', array('parent' => 0));
                            if ($myterm->name == $product_terms[0]->name) {
                                $curr_taxonomy_posts[] = $post;
                            }
                        }
                        //-- prendo i post della tassonomia corrente
                        foreach ($curr_taxonomy_posts as $curr) {
                            $level = 0;
                            recursiveItems($curr, $level);
                        }
                        ?>
                    </div>
                    <?php
                    $active = "";
                }
                ?>
            </div>

        </div>   <!-- #content -->
    </div> <!-- #primary -->
<?php } else {
    echo "<script language='javascript'>\n";
    echo "  location.href=\"".home_url('/login')."\"";
    echo "</script>\n";
}

get_footer(); ?>

<script type="text/javascript">
    $('.menu_download a.logic-btn').click(function () {
        var $this = $(this);
        $('.menu_download a.logic-btn').removeClass("active");
        $this.addClass("active");
        $('div.section').hide("slow", function () {
            $('div#' + $this.attr('data-id')).show("slow");
        });
    });
</script>
<?php /*
Template Name: Azienda: Luce ai dettagli template
*/
?>
<?php get_header(); ?>

<div class="container">
<?php while ( have_posts() ) : the_post(); ?>

    <?php 

	// check if the flexible content field has rows of data
if( have_rows('elementi') ):
     // loop through the rows of data
    while ( have_rows('elementi') ) : the_row(); 
		$image1 = get_sub_field('img1');
		//$image2 = get_sub_field('img2');
		$size1 = 'pensiero_small';
		$size2 = 'pensiero_quad';
		$Link_type = get_sub_field('link');
		if( $Link_type === 'Link interno' ):
		$the_link = get_sub_field('link_interno');
		$target = '';
		else:
		$the_link = get_sub_field('link_esterno');
		$target = ' target="_blank"';
		endif;
	?>
		<div class="col-xs-12 blocco luce clearfix" >
        	<!-- modello_1 -->
			<?php if( get_row_layout() == 'modello_1' ): ?>
			<?php if($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
              <?php }?>
              <div class="row">
                <div class="col-sm-6">
                    <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div >
                    <?php 
                    echo wp_get_attachment_image( $image1, $size2 );
                    ?>
                    </div>
                </div>
              </div>
            <?php if($the_link){ ?>
			</a>
              <?php }?>
			<!-- modello_2 -->
			<?php elseif( get_row_layout() == 'modello_2' ): ?>
			<?php if($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
              <?php }?>
              <div class="row">
                <div class="col-sm-6">
                    <?php 
                    $image1 = get_sub_field('img1');
                    echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                </div>
                <div class="col-sm-6">
                    <div class="txt txt2_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
                </div>
              </div>
            <?php if($the_link){ ?>
			</a>
              <?php }?>
           <?php endif; // get_row_layout() ==?>
          </div><!-- .blocco -->

		
    <?php  endwhile; ?>
	<?php endif;
	endwhile;
	?>
</div>
<!-- #container -->

<?php get_footer(); ?>

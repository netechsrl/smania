<?php /*
Template Name: Materiale (single)
*/
?>
<?php get_header('page'); ?>

<?php while ( have_posts() ) : the_post(); ?>
<div class="container">

    <?php 

	// check if the flexible content field has rows of data
if( have_rows('elementi') ):
     // loop through the rows of data
    while ( have_rows('elementi') ) : the_row(); 
		$image1 = get_sub_field('img1');
		$image2 = get_sub_field('img2');
		$size1 = 'pensiero_small';
		$size2 = 'mymood_small';
		$size3 = 'pensiero_quad';
		$Link_type = get_sub_field('link');
		if( $Link_type === 'Link interno' ):
		$the_link = get_sub_field('link_interno');
		$target = '';
		else:
		$the_link = get_sub_field('link_esterno');
		$target = ' target="_blank"';
		endif;
	?>
		<div class="col-xs-12 blocco materiale clearfix" >
			<?php if( get_row_layout() == 'modello_1' ): ?>
        	<!-- modello_1 -->
              <div class="row">
                <div class="col-md-6">
                    <div >
                    <?php 
                    echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                    </div>
                    <div class="img_small_down">
                    <?php 
                    $image1 = get_sub_field('img1');
                    echo wp_get_attachment_image( $image2, $size2 );
                    ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
                </div>
              </div>
			<?php elseif( get_row_layout() == 'modello_2' ): ?>
			<!-- modello_2 -->
              <div class="row">
                <div class="col-md-6">
                    <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div >
                    <?php 
                    echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                    </div>
                    <div class="img_small_down">
                    <?php 
                    $image1 = get_sub_field('img1');
                    echo wp_get_attachment_image( $image2, $size2 );
                    ?>
                    </div>
                </div>
              </div>
           <?php elseif( get_row_layout() == 'video' ): ?>
           <!-- video -->
		   <style>
                .embed-container { 
                    position: relative; 
                    padding-bottom: 56.25%;
                    height: 0;
                    overflow: hidden;
                    max-width: 100%;
                    height: auto;
                } 
            
                .embed-container iframe,
                .embed-container object,
                .embed-container embed { 
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                }
            </style>
              <div class="row" style="padding: 0 5px;">
                <div class="embed-container">
					<?php the_sub_field('video_url'); ?>
				</div>
              </div>
           <?php endif; // get_row_layout() ==?>
          </div><!-- .blocco -->

		
    <?php  endwhile; ?>
	<?php endif;
	endwhile;
	?>
    
        <?php 
	//if( have_rows('finiture_ispirazioni') ){
		//while ( have_rows('finiture_ispirazioni') ) {
		//the_row();
        $img_finiture = get_field('finiture_ispirazioni');
		if($img_finiture){ ?>
  <section id="ispiraz_finiture">
  	
    <?php 
	foreach( $img_finiture as $color ){ 
		if(get_field('sel_finiture', $color->ID)){
		$finitura = get_field('sel_finiture', $color->ID);
		$scheda = get_field('scheda_pdf', $finitura->ID);
		$tit = get_the_title($finitura->ID);
		$tipo = get_field('tipo_finitura', $finitura->ID);
		$titolo = $tit.' '.$tipo;
		$sottotitolo = get_the_title($color->ID);
		}else{
		//$finitura = get_field('sel_finiture', $color->ID);
		$scheda = get_field('scheda_pdf', $color->ID);
		$tit = get_the_title($color->ID);
		$tipo = get_field('tipo_finitura', $color->ID);
		$titolo = $tit.' '.$tipo;
		$sottotitolo = '';
		}
	?>
    <div class="finiture_grid" style="max-width:136px;">
    	<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="<?php echo get_the_post_thumbnail_url( $color->ID, 'full' ); ?>">
        	<?php echo get_the_post_thumbnail( $color->ID, 'thumbnail' ); ?>
        </a>
         <div class="col-xs-9" style="padding:0 4px;">
         	<h1><?php echo $titolo ?></h1>
         	<p><?php echo $sottotitolo ?></p>
         </div>
         <div class="col-xs-3" style="padding:8px 0;">
         <?php if ($scheda){ ?>
         <a href="<?php echo $scheda ?>" target="_blank" style="padding:0;"><img style=" padding-left:10px; width:auto; float:none;" src="<?php echo get_stylesheet_directory_uri() ?>/img/icn-scheda_tecnica.png"></a>
         <?php }?>
         </div>
    </div>
    <?php 
		} // foreach( $img_finiture as $color )
	?>
  
  </section>
  <?php } //if($img_finiture) ?>

    
    
</div>
<!-- #container -->

<?php get_footer(); ?>

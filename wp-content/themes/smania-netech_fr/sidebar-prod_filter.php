<?php

/*$taxonomy     = 'ambiente';
$orderby      = 'name';
$show_count   = false;
$pad_counts   = false;
$hierarchical = true;
$title        = '';

$args = array(
    'taxonomy'     => $taxonomy,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title,
    'hide_empty'   => true,
);*/

?>

<style>
    .panel-heading .accordion-toggle:after {
        font-family: 'FontAwesome';
        content: "\f107";
        float: right;
        color: #a46e24;
    }

    .panel-heading .accordion-toggle.collapsed:after {
        content: "\f105";
    }
	
	
	.regular-checkbox {
	display: none;
}

.regular-checkbox + label {
	background-color: rgba(164,111,37,.1)/*#fafafa*/;
	border: 1px solid rgba(164,111,37,.2);
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
	padding: 7px;
	border-radius: 0;
	display: inline-block;
	position: relative;
}

.regular-checkbox + label:active, .regular-checkbox:checked + label:active {
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}

.regular-checkbox:checked + label {
	background-color: rgba(164,111,37,.15);
	border: 1px solid rgba(164,111,37,.2);
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
	color: #a46e24;
}

.regular-checkbox:checked + label:after {
	content: '\2715';
	font-size: 13px;
	position: absolute;
	top: 0px;
	left: 3px;
	color: #a46e24;
}


	
</style>

<div class="panel-group" id="accordion">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse"
                                       href="#ambiente"><?php echo __('ambienti') ?></a></h4>
        </div>
        <div id="ambiente" class="panel-collapse collapse in filter-pnl">
            <?php
            $ambienti = get_terms(array(
                'taxonomy'   => 'ambiente',
                'hide_empty' => false,
            ));
            if ( ! empty($ambienti) && ! is_wp_error($ambienti)) {
                //echo '<div id="amb">';
                echo '<ul id="amb" class="cat-prodotto" style="margin-top:10px">';
                foreach ($ambienti as $ambiente) {
                    echo '<li><input id="'.$ambiente->slug.'" class="isotofilter regular-checkbox" type="checkbox" data-filter=".'.$ambiente->slug.'"><label for="'.$ambiente->slug.'" ></label>'.$ambiente->name.'</li>';
                }
                echo '</ul>';
                //echo '</div>';
            }
            ?>
        </div>
    </div>

    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse"
                                       href="#collezione"><?php echo __('collezione') ?></a></h4>
        </div>
        <div id="collezione" class="panel-collapse collapse in filter-pnl">
            <?php
            $collezioni = get_terms(array(
                'taxonomy'   => 'collezione',
                'hide_empty' => false,
            ));
            if ( ! empty($collezioni) && ! is_wp_error($collezioni)) {
                //echo '<div id="amb">';
                echo '<ul id="coll" class="cat-prodotto" style="margin-top:10px">';
                foreach ($collezioni as $collezione) {
                    echo '<li><input id="'.$collezione->slug.'" class="isotofilter regular-checkbox" type="checkbox" data-filter=".'.$collezione->slug.'"><label for="'.$collezione->slug.'" ></label>'.$collezione->name.'</li>';
                }
                echo '</ul>';
                //echo '</div>';
            }
            ?>
        </div>
    </div>

    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse"
                                       href="#arredi"><?php echo __('arredi') ?></a></h4>
        </div>
        <div id="arredi" class="panel-collapse collapse in filter-pnl">
            <?php
            $arredi = get_terms(array(
                'taxonomy'   => 'arredo',
                'hide_empty' => false,
            ));
            if ( ! empty($arredi) && ! is_wp_error($arredi)) {
                //echo '<div id="amb">';
                echo '<ul id="arr" class="cat-prodotto" style="margin-top:10px">';
                foreach ($arredi as $arredo) {
                    echo '<li><input id="'.$arredo->slug.'" class="isotofilter regular-checkbox" type="checkbox" data-filter=".'.$arredo->slug.'"><label for="'.$arredo->slug.'" ></label>'.$arredo->name.'</li>';
                }
                echo '</ul>';
                //echo '</div>';
            }
            ?>
        </div>
    </div>


</div>



<?php get_header('page'); ?>

<div class="container">
  <div id="primary">
    <div id="content" class="col-sm-9 " role="main">
      <?php $i = 0; ?>
      <?php while ( have_posts() ) : the_post(); 
  
  ?>
      <?php if($i % 4 == 0) { ?>
      <div class="row" style="padding:20px 0;">
        <?php }?>
        <a href="<?php the_permalink() ?>">
        <div class="col-xs-6 col-md-3 item-grid">
          <?php the_post_thumbnail('prodotto') ?>
          <p class="grid-tit">
            <?php the_title();?>
          </p>
        </div>
        </a>
        <?php $i++;
  if($i != 0 && $i % 4 == 0) { ?>
      </div>
      <div class="clearfix"></div>
      <?php }
  
  ?>
      <?php endwhile; // end of the loop. ?>
    </div>
  </div>
  <!-- #content -->
  <div class="col-sm-3">
    <?php  get_sidebar('prodotti'); ?>
  </div>
</div>
<!-- #primary -->
</div>
<!-- / .container -->
<?php get_footer(); ?>

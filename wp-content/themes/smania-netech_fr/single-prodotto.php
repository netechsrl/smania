<?php get_header(); ?>

<div id="primary" class="pag_prodotto">
  <div id="content" role="main">
    <?php while ( have_posts() ) : the_post(); ?>
<div class="container">

    <div class="row">
      <div class="col-md-6">
        <h2>
          <?php the_title(); ?>
          <?php if (get_field('codice')): ?>
          <span style="font-family: 'Roboto', sans-serif; font-weight:600; font-size:small;"> - cod. <span style="text-transform:uppercase; font-weight:400; color:#999;"><?php echo get_field('codice') ?>
          </span></span><?php endif?></h2>
        <div>
          <?php the_content(); ?>
        </div>
      </div>
      <div class="col-md-6">
      	<div class="img_prodotto">
        <a class="gallery_image nivo" href="<?php the_post_thumbnail_url( 'full' ); ?>">
			<img src="<?php echo get_the_post_thumbnail_url(); ?>">
        </a>
        </div>
      </div>
    </div>
    <!-- / .row -->
  </div><!-- / .container -->
    
    <div class="row background_row_1" style="padding-bottom: 80px;">
    <div class="container">
      <div class="col-md-3">
      	<div>
      	<?php if(get_field('scheda_tecnica')){?>
        	<span style="text-transform:uppercase; font-weight:600;"><a href="<?php echo get_field('scheda_tecnica'); ?>" target="_blank"><?php echo __('Download datasheet') ?><img style=" padding-left:10px;" src="<?php echo get_stylesheet_directory_uri() ?>/img/icn-scheda_tecnica.png"></a></span>
        <?php }?>
        </div>
        <div>
      	<?php //if(get_field('social', 'option')){?>
        	<ul class="social_share">
        		<li>
                <span style="text-transform:uppercase; font-weight:600;"><?php echo __('Share') ?></span>
                </li>
                <span style="font-size: large;">
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" rel="nofollow" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/home?status=<?php the_permalink() ?>" rel="nofollow" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" rel="nofollow" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                </span>
            </ul>
        <?php //}?>

        
        </div>
        
      </div>
      <div class="col-md-9">
      	<div class="col-md-7" style="text-align:right;">
        	<?php the_field('dettagli_testo');?>
        </div>
      	<div class="col-md-5">
   <?php $images = get_field('dettagli_immagini');
	if( $images ):  
	if (count($images) >1){
	?>
        <div class="prod_gallery">
          
            <div class="swiper-container">
              <div class="swiper-wrapper">
                <?php foreach( $images as $image ): 
						$content = '<div class="swiper-slide">';
							$content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
								 $content .= '<img src="'. $image['sizes']['prod_gallery'] .'" alt="'. $image['alt'] .'"  />';
							$content .= '</a>';
						$content .= '</div>';
						if ( function_exists('slb_activate') ){
						$content = slb_activate($content);
						}
						echo $content;
						endforeach; ?>
              </div>
              <?php //if (count($images) >3){?>
              <!--Add Pagination --> 
                <div class="swiper-pagination"></div>
              <!--Add Navigation -->
              <div class="swiper-button-prev swiper-button-white"></div>
              <div class="swiper-button-next swiper-button-white"></div>
              <?php //}?>
            </div>
          
        </div>
       <?php }else{
			
			echo '<img src="'. $images[0]['sizes']['prod_gallery'] .'" alt="'. $image['alt'] .'"  />';
			}
         endif; ?>
        </div>
      </div>
      </div><!-- / .container -->
    </div>  <!-- / .row .background_row_1-->
    <!-- Tabs -->
			<script type="text/javascript">
        function rowColor() {
            var property = document.getElementById('row_tab');
            property.className = 'tab_background' == property.className ? '' : 'tab_background';
        }
        </script>
    <div id="row_tab" class="row">
    	<div class="container">
            <ul class="nav nav-tabs prod-tabs" style=" margin-top:-30px;">
              <li class="mymood active"><a data-toggle="tab" href="#tab_mymood" onclick="rowColor()">myMood</a></li>
              <li class="preventivo"><a data-toggle="tab" href="#tab_preventivo" onclick="rowColor()"><?php echo __('richiedi un preventivo') ?></a></li>
            </ul>
            
            <div class="tab-content">
              <div id="tab_mymood" class="tab-pane fade in active mymood">
              <?php 
			  	if( have_rows('inserisci_materiali') ){
					$colors = new WP_Query( array( 'post_type' => 'colore-finitura') );
					while ( have_rows('inserisci_materiali') ) {
					 the_row();
			  ?>
                <section id="sez_materiale" style="clear:both;">
                    <p><?php echo get_sub_field('testo_personalizzazione'); ?></p>
                    <?php 
                    $materiale = get_sub_field('materiale', $post->ID);
                    $tit = strtoupper(get_field('etichetta', $materiale->ID));
                    $finiture = get_sub_field('finiture', $post->ID);
                    ?>
                    <h4><?php echo $tit; ?></h4>
                    <?php 
                    if( $finiture ){ ?>
                        
                        <?php foreach( $finiture as $finitura ){  
                        ?>    
                            <div class="finiture_grid" data-id='fin-<?php echo $finitura->ID ?>' style="cursor:pointer;">
                                <?php echo get_the_post_thumbnail( $finitura->ID, 'thumbnail' ); ?>
                                <h1><?php echo get_the_title( $finitura->ID ); ?></h1>
                                <p><?php echo get_field( 'tipo_finitura', $finitura->ID ); ?></p>
                            </div>
                            
                            
                        <?php }//foreach( $finiture as $finitura ) ?>
                        
                    <?php }//if( $finiture ) ?>
                    
                    
              </section>
              <section id="sez_colori" style="clear:both;">
              <?php foreach( $finiture as $finitura ){  
                        ?>  
              				
                            <div id="fin-<?php echo $finitura->ID ?>" class="colori chiuso" >
                            	<div class="titolo">
                                	<div style="float:left;"><?php echo get_the_title( $finitura->ID )?><?php if(get_field( 'tipo_finitura', $finitura->ID )){ echo ' - <span>'.get_field( 'tipo_finitura', $finitura->ID ).'</span>'; }; ?> </div>
                                    <div style="float:right; padding-right:20px;"><a href="javascript:" data-id="fin-<?php echo $finitura->ID ?>">x</a></div>
                                </div>
                            	<div style="float:left; margin-right:4px;">
								<?php echo get_the_post_thumbnail( $finitura->ID, 'quad' ); ?>
                                <h1><?php echo get_the_title( $finitura->ID ).' '.get_field( 'tipo_finitura', $finitura->ID ); ?> </h1>
                                <?php if (get_field( 'scheda_pdf',$finitura->ID )){ ?>
                                	<p><a href="<?php echo get_field( 'scheda_pdf', $finitura->ID) ?>" target="_blank" style="padding:0;"><?php echo __('scarica la scheda materiale') ?> <img style=" padding-left:10px; width:auto; float:none;" src="<?php echo get_stylesheet_directory_uri() ?>/img/icn-scheda_tecnica.png"></a></p>
                                <?php }?>
                                </div>
                                
                                <?php 
								$fin_ID =  $finitura->ID  ;
								
								while ($colors->have_posts()) {
									$colors->the_post();
									$sel =  get_field('sel_finiture');
									$sel_finiture_ID = $sel->ID ;
								?>
                                
                                
                                <?php 
									if($sel_finiture_ID == $fin_ID){
								?>
                                	<div style="float:left; margin:0 4px 4px;">
                                     
									<img src="<?php echo get_the_post_thumbnail_url($colors->ID); ?>" style="width:100px;" />
									<p><?php echo get_the_title( $colors->ID );?></p>
								
									
                                    
                                    </div>
                                <?php 
									}
								}?>
                                <?php wp_reset_query(); ?>
                            </div>
                            <?php }?>
                </section>
              <?php 
					} //while ( have_rows('inserisci_materiali') )
				
				} //if( have_rows('inserisci_materiali') )
			  
			  ?>
                
              </div>
              <div id="tab_preventivo" class="tab-pane fade in preventivo" style="background: #a46e24;margin: -10px 0 0; padding:30px">
                <div class="col-xs-12">
                    <h4><?php echo __('richiedi un preventivo per') ?> <?php the_title(); ?> (cod. <?php echo get_field('codice') ?>)</h4>
                    <p><?php echo __('Scegli le finiture e compila il form.') ?></p>
                </div>
                <div class="col-sm-3">
                
                </div>
                <div class="col-sm-9">
                	<?php 
					$modulo = 'Modulo contatti_'.ICL_LANGUAGE_CODE;
					
					echo do_shortcode( '[contact-form-7 title="'.$modulo.'"]' ); ?>
                </div>
                
              </div>
            </div>
        </div>
    </div>
    <!-- / Tabs -->

    
    <?php endwhile; // end of the loop. ?>
  </div>
  <!-- #content --> 
</div>
<!-- #primary -->
  <script type="text/javascript">
      jQuery('.finiture_grid').click(function () {
          var div_btn = jQuery(this);
		  jQuery('.colori').hide("slow");
          //jQuery('#' + div_btn.attr('data-id')).removeClass("chiuso");
          jQuery('#' + div_btn.attr('data-id')).show("slow");
        
          
      });
	  
	  jQuery('#sez_colori .titolo div a').click(function () {
          var div_btn_close = jQuery(this);
		  //jQuery('.colori').hide("slow");
          //jQuery('#' + div_btn.attr('data-id')).removeClass("chiuso");
          jQuery('#' + div_btn_close.attr('data-id')).hide("slow");
        
          
      });
	  
	  
  </script>

<?php get_footer(); ?>

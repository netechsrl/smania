<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php wp_title('&raquo;', 'true', 'right'); ?>
        <?php bloginfo('name'); ?>
    </title>
    <meta name="description" content="<?php //echo get_option('my_description'); ?>"/>

    <!-- Favicon -->
    <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/lib/yamm/yamm.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="<?php echo get_stylesheet_directory_uri(); ?>/lib/nivo-lightbox/dist/nivo-lightbox.min.css">
    <link rel="stylesheet"
          href="<?php echo get_stylesheet_directory_uri(); ?>/lib/nivo-lightbox/themes/default/default.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lib/Swiper/dist/css/swiper.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/custom.style.css" rel="stylesheet">

    <!-- Google web Font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,100' rel='stylesheet' type='text/css'>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>

    <style>
        #map {
            height: 600px;
        }

        .filter-pnl {
            padding: 5px;
        }

        .filter-pnl .container {
            width: 100%;
            background-color: #a46e24;
			padding-bottom: 40px;
        }

        .rivenditori-list {
            padding: 5px;
            overflow-y: auto !important;
            height: 500px !important;
        }

        .rivenditori-list .container {
            background-color: #eae4d1;
            width: 100%;
            min-height: 99%;
        }

        #filterRivenditori {
            padding: 40px;
        }

        #filterRivenditori .btn-default {
            background: #eae4d1 none repeat scroll 0 0;
            color: #a46e24;
            font-weight: bold;
            padding: 15px 20px;
            text-transform: uppercase;
            width: 100%;
            border-radius: 0px;
        }

        .styled-select {
            /*background: url(../images/select-dropdown-icon.png) no-repeat scroll 96% center #FFF;*/
            border: 1px solid #eae4d1;
            height: 44px;
            overflow: hidden;
            width: 100%;
            position: relative;
            margin-bottom: 30px;
        }

        .styled-select select {
            background: transparent none repeat scroll 0 0;
            border: 0 none;
            border-radius: 0;
            color: #eae4d1;
            font-size: 15px;
            height: 44px;
            line-height: 1;
            padding: 0 10px;
            width: 120%;
        }

        .gmaps_marker {
            float: left;
            margin-top: -15px;
        }

        .col-md-1.marker {
            margin-right: 10px;
        }

        .no-display {
            display: none;
        }

        .nome_riv {
            text-transform: uppercase;
            color: #a46e24;
            font-weight: bold;
        }

        .container.body > .row {
            padding: 0px;
            padding-top: 15px;
        }

        .row.rivenditore {
            width: 100%;
        }

        @media only screen and (min-width: 320px) {
            #filterRivenditori {
                padding: 20px 10px !important;
            }

            .row.rivenditore {
                margin: 0 !important;
                padding: 20px 0 !important;
            }
        }

        @media only screen and (min-width: 992px) {
            .row.rivenditore {
                padding: 40px 0 !important;
            }
        }
    </style>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1286086064837921'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1286086064837921&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- search button -->
            <div class="navbar-header nav-search">
                <button type="button" class="navbar-search_btn  dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true" id="search-form">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="search-form" style="opacity:.7;">
                    <form role="search" method="get" class="search-field" action="<?php echo home_url('/'); ?>">
                        <input type="search" name="s" id="s" value="" placeholder="<?php echo __('Search...'); ?>"
                               class="ui-autocomplete-input" autocomplete="off"
                               style="font-size: 30px; text-align: center;">
                    </form>


                </div>
            </div>
            <!-- search button -->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="mobile-view">
                <div class="navbar-header" style="z-index:200">
                    <div style="position: absolute; top: 0; left: 0;">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#menu-mobile" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="logo">
                        <?php $logo = get_field('logo', 'option'); ?>
                        <a class="navbar-brand"
                           href="<?php echo home_url('/'); ?>"><?php echo wp_get_attachment_image($logo, 'logo'); ?></a>
                    </div>
                </div>

                <!-- menu mobile -->
                <div class="collapse navbar-collapse" id="menu-mobile">
                    <?php
                    wp_nav_menu(array(
                        'menu'           => 'mobile',
                        'theme_location' => 'mobile',
                        'depth'          => 4,
                        //                        'container'       => 'div',
                        //                        'container_class' => 'collapse navbar-collapse',
                        'container_id'   => 'bs-example-navbar-collapse-3',
                        'menu_class'     => 'nav navbar-nav',
                        'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
                        'walker'         => new wp_bootstrap_navwalker(),
                    ));
                    ?>
                </div><!-- /.navbar-collapse mobile -->
            </div> <!-- / .mobile-view-->

            <div class="desktop-view">
                <div class="navbar-header" style="z-index:200">
                    <!--                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"-->
                    <!--                            data-target="#menu-secondary" aria-expanded="false">-->
                    <!--                        <span class="sr-only">Toggle navigation</span>-->
                    <!--                        <span class="icon-bar"></span>-->
                    <!--                        <span class="icon-bar"></span>-->
                    <!--                        <span class="icon-bar"></span>-->
                    <!--                    </button>-->
                    <!-- menu secondary -->
                    <div id="menu-secondary">
                        <?php
                        wp_nav_menu(array(
                            'menu'            => 'secondary',
                            'theme_location'  => 'secondary',
                            'depth'           => 4,
                            'container'       => 'div',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id'    => 'bs-example-navbar-collapse-2',
                            'menu_class'      => 'nav navbar-nav yamm navbar-right',
                            'fallback_cb'     => 'Yamm_Nav_Walker_menu_fallback',
                            'walker'          => new Yamm_Fw_Nav_Walker(),
                        ));
                        ?>
                    </div><!-- /.navbar-collapse secondary -->
                    <?php $logo = get_field('logo', 'option'); ?>
                    <a class="navbar-brand"
                       href="<?php echo home_url('/'); ?>"><?php echo wp_get_attachment_image($logo, 'logo'); ?></a>
                </div>

                <div class="desktop-main-menu">
                    <?php
                    wp_nav_menu(array(
                        'menu'            => 'primary',
                        'theme_location'  => 'primary',
                        'depth'           => 4,
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id'    => 'bs-example-navbar-collapse-1',
                        'menu_class'      => 'nav navbar-nav yamm navbar-right',
                        'fallback_cb'     => 'Yamm_Nav_Walker_menu_fallback',
                        'walker'          => new Yamm_Fw_Nav_Walker(),
                    ));
                    ?>
                </div>
            </div> <!-- / .desktop-view-->


        </div><!-- /.container-fluid -->
    </nav>
    <!-- Image header -->
    <?php
    $img_header = get_field('img_header', 'option');
    global $post;
    $content = $post->post_content;

    //$page_id = get_the_id();
    if (is_archive()):
        //$obj = get_post_type_object( 'prodotto' );
        $title_page = post_type_archive_title('', false);
        $content    = '';
    else:
        $title_page = get_the_title();
    endif;
    ?>

    <div id="map"></div>

    <script>

        var SZPlaces = {
            markers: [],
            bindMarkers: [],
            init: function () {
                jQuery(function () {
                    jQuery('.rivenditore').not('.isotope-hidden').each(function (k, el) {
                        SZPlaces.markers.push({
                            pos: jQuery(el).data('latlng'),
                            img: jQuery(el).data('img'),
                            title: jQuery(el).data('title')
                        });
                    });
                });

                // When the window has finished loading create our google map below
                google.maps.event.addDomListener(window, 'load', SZPlaces.initMap2);
            },
            removeAllMArkers: function () {
                for (var i = 0; i < SZPlaces.bindMarkers.length; i++) {
                    SZPlaces.bindMarkers[i].setMap(null);
                }
                SZPlaces.markers = [];
            },
            initMap: function () {

                var mapOptions = {
                    zoom: 2,
                    scrollwheel: false,     //zoom when scroll
                    draggable: true,
                    center: new google.maps.LatLng(46.086156, 11.987908), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: /*[{"stylers":[{"hue":"#000000"},{"weight":0.5},{"gamma":0.5}]},{"elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","stylers":[{"color":"#73AD97"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#205698"},{"visibility":"on"},{"weight":0.5}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"transit","stylers":[{"color":"#D9E5F2"},{"lightness":5}]},{"featureType":"poi","stylers":[{"color":"#806f59"},{"lightness":5}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"},{"gamma":1.14},{"saturation":-18}]},{"featureType":"road.highway.controlled_access","elementType":"labels","stylers":[{"saturation":30},{"gamma":0.76}]},{"featureType":"road.local","stylers":[{"visibility":"simplified"},{"weight":0.4},{"lightness":-8}]},{"featureType":"water","stylers":[{"color":"#BAD1E6"}]},{"featureType":"landscape.man_made","stylers":[{"color":"#8c8c8c"}]},{"featureType":"poi.business","stylers":[{"saturation":68},{"lightness":-61}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"weight":2.7},{"color":"#f4f9e8"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#376E54"},{"lightness":14},{"weight":1.4}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"weight":1},{"color":"#F4F4F4"},{"saturation":-42},{"lightness":28}]}]*/
                        [{"stylers": [{"hue": "#2c3e50"}, {"saturation": 250}]}, {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [{"lightness": 50}, {"visibility": "simplified"}]
                        }, {"featureType": "road", "elementType": "labels", "stylers": [{"visibility": "off"}]}]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');
                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);
                var listener = google.maps.event.addListener(map, "idle", function () {
                    if (map.getZoom() > 8) map.setZoom(8);
                    google.maps.event.removeListener(listener);
                });
                var infoWindow = new google.maps.InfoWindow({});

                // Let's also add a marker while we're at it
                var latlngbounds = new google.maps.LatLngBounds();
                var hqs = [];
                $.each(SZPlaces.markers, function (k, e) {
                    var latng = e.pos.split(",");
                    latng = new google.maps.LatLng(latng[0], latng[1]);
                    latlngbounds.extend(latng);
                    var marker = new google.maps.Marker({
                        position: latng,
                        map: map,
                        title: e.title,
                        icon: markerBase + 'map-marker-' + e.type + '.png'
                    })
                    google.maps.event.addListener(marker, 'click', function () {
                        infoWindow.setContent('<h5>' + e.title + '</h5><hr>' + e.desc);
                        infoWindow.open(map, marker);
                    });
                    if (e.type == 'hq')
                        new google.maps.event.trigger(marker, 'click');

                });
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);

            },
            initMap2: function () {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 2,
                    scrollwheel: false,
                    draggable: true,
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(46.086156, 11.987908), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: //                        [{
//                        "featureType": "landscape.man_made",
//                        "elementType": "geometry",
//                        "stylers": [{"color": "#f7f1df"}]
//                    }, {
//                        "featureType": "landscape.natural",
//                        "elementType": "geometry",
//                        "stylers": [{"color": "#d0e3b4"}]
//                    }, {
//                        "featureType": "landscape.natural.terrain",
//                        "elementType": "geometry",
//                        "stylers": [{"visibility": "off"}]
//                    }, {
//                        "featureType": "poi",
//                        "elementType": "labels",
//                        "stylers": [{"visibility": "off"}]
//                    }, {
//                        "featureType": "poi.business",
//                        "elementType": "all",
//                        "stylers": [{"visibility": "off"}]
//                    }, {
//                        "featureType": "poi.medical",
//                        "elementType": "geometry",
//                        "stylers": [{"color": "#fbd3da"}]
//                    }, {
//                        "featureType": "poi.park",
//                        "elementType": "geometry",
//                        "stylers": [{"color": "#bde6ab"}]
//                    }, {
//                        "featureType": "road",
//                        "elementType": "geometry.stroke",
//                        "stylers": [{"visibility": "off"}]
//                    }, {
//                        "featureType": "road",
//                        "elementType": "labels",
//                        "stylers": [{"visibility": "off"}]
//                    }, {
//                        "featureType": "road.highway",
//                        "elementType": "geometry.fill",
//                        "stylers": [{"color": "#ffe15f"}]
//                    }, {
//                        "featureType": "road.highway",
//                        "elementType": "geometry.stroke",
//                        "stylers": [{"color": "#efd151"}]
//                    }, {
//                        "featureType": "road.arterial",
//                        "elementType": "geometry.fill",
//                        "stylers": [{"color": "#ffffff"}]
//                    }, {
//                        "featureType": "road.local",
//                        "elementType": "geometry.fill",
//                        "stylers": [{"color": "black"}]
//                    }, {
//                        "featureType": "transit.station.airport",
//                        "elementType": "geometry.fill",
//                        "stylers": [{"color": "#cfb2db"}]
//                    }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#a2daf2"}]}]

                        [{
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [{"color": "#e4e4e4"}]
                        }, {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [{"color": "#f9f9f9"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [{"color": "#bfbfbf"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [{"color": "#a6a6a6"}]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [{"color": "#bfbfbf"}, {"lightness": -20}]
                        }, {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [{"color": "#bfbfbf"}, {"lightness": -17}]
                        }, {
                            "elementType": "labels.text.stroke",
                            "stylers": [{"color": "#808080"}, {"visibility": "on"}, {"weight": 0.6}]
                        }, {
                            "elementType": "labels.text.fill",
                            "stylers": [{"visibility": "on"}, {"color": "#808080"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "labels",
                            "stylers": [{"visibility": "simplified"}]
                        }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [{"color": "#bfbfbf"}, {"lightness": -10}]
                        }, {
                            "featureType": "administrative",
                            "elementType": "geometry",
                            "stylers": [{"color": "#bfbfbf"}, {"weight": 0.7}]
                        }]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');
                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var latlngbounds = new google.maps.LatLngBounds();
                var hqs = [];
                $.each(SZPlaces.markers, function (k, e) {
                    var latng = e.pos.split(",");
                    latng = new google.maps.LatLng(latng[0], latng[1]);
                    latlngbounds.extend(latng);
                    var marker = new google.maps.Marker({
                        position: latng,
                        map: map,
                        title: e.title,
                        // icon: markerBase + 'map-marker-' + e.type + '.png'
                        icon: e.img
                    });
                    SZPlaces.bindMarkers.push(marker);
                    google.maps.event.addListener(marker, 'click', function () {
                        infoWindow.setContent('<h5>' + e.title + '</h5><hr>' + e.desc);
                        infoWindow.open(map, marker);
                    });
                    // if (e.type == 'hq')
                    //     new google.maps.event.trigger(marker, 'click');

                });
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);

            }
        };

        SZPlaces.init();

    </script>

    <?php /***** Breadcrumbs *****/
    if ( ! is_front_page()) {

        if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<nav class="breadcrumb" id="breadcrumbs"><div class="container" style="padding-left:40px;">',
                '</div></nav>');
        }

    } // if ( !is_front_page())
    /***** end Breadcrumbs *****/ ?>


</header>
	



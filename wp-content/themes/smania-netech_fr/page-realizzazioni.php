<?php /*
Template Name: Realizzazioni template
*/
?>
<?php get_header(); ?>

<div class="container">
<?php while ( have_posts() ) : the_post(); ?>

    <?php 

	// check if the flexible content field has rows of data
if( have_rows('elementi') ):
     // loop through the rows of data
    while ( have_rows('elementi') ) : the_row(); 
		$image1 = get_sub_field('img1');
		$size = 'realizzazioni_big';
		$Link_type = get_sub_field('link');
		if( $Link_type === 'Link interno' ):
		$the_link = get_sub_field('link_interno');
		$target = '';
		else:
		$the_link = get_sub_field('link_esterno');
		$target = ' target="_blank"';
		endif;
	?>
		<div class="col-xs-12 blocco realizzazioni clearfix" >
        	<!-- modello_1 -->
			<?php if( get_row_layout() == 'modello_1' ): ?>
              <div class="row">
                <div class="col-md-3">
            <?php if ($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
            <?php }?>
                    <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
            <?php if ($the_link){ ?>  
            </a>
            <?php }?>
                </div>
                <div class="col-md-9">
                    <?php 
                    //echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                    
                    <div>
					   <?php $images = get_sub_field('gallery');
                        if( $images ):  
						?>
                        	<?php if (count($images) >1){?>
                            <div class="blocco_gallery">
                                <div class="swiper-container">
                                  <div class="swiper-wrapper">
                                    <?php foreach( $images as $image ): 
                                            $content = '<div class="swiper-slide">';
                                                $content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
                                                     $content .= '<img src="'. $image['sizes'][$size] .'" alt="'. $image['alt'] .'"  />';
                                                $content .= '</a>';
                                            $content .= '</div>';
                                            if ( function_exists('slb_activate') ){
                                            $content = slb_activate($content);
                                            }
                                            echo $content;
                                            endforeach; ?>
                                  </div>
                                 
                                  <!--Add Pagination --> 
                            		<div class="swiper-pagination"></div>
                                  <!--Add Navigation -->
                                  <div class="swiper-button-prev swiper-button-white"></div>
                                  <div class="swiper-button-next swiper-button-white"></div>
                                  <?php //}?>
                                </div>
                              
                            </div>
                          <?php }else{
							  	
                                echo '<img src="'. $images[0]['sizes'][$size] .'" alt="'. $image['alt'] .'"  />';
                                }?>
                                
                      <?php 
					  endif; ?>
                    </div>
                    
                </div>
              </div>
              
			<!-- modello_2 -->
			<?php elseif( get_row_layout() == 'modello_2' ): ?>
              <div class="row">
                <div class="col-md-9">
                    <?php 
                    //echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                    
                    <div>
					   <?php $images = get_sub_field('gallery');
                        if( $images ):  
						?>
                        	<?php if (count($images) >1){?>
                            <div class="blocco_gallery">
                                <div class="swiper-container">
                                  <div class="swiper-wrapper">
                                    <?php foreach( $images as $image ): 
                                            $content = '<div class="swiper-slide">';
                                                $content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
                                                     $content .= '<img src="'. $image['sizes'][$size] .'" alt="'. $image['alt'] .'"  />';
                                                $content .= '</a>';
                                            $content .= '</div>';
                                            if ( function_exists('slb_activate') ){
                                            $content = slb_activate($content);
                                            }
                                            echo $content;
                                            endforeach; ?>
                                  </div>
                                 
                                  <!--Add Pagination --> 
                            		<div class="swiper-pagination"></div>
                                  <!--Add Navigation -->
                                  <div class="swiper-button-prev swiper-button-white"></div>
                                  <div class="swiper-button-next swiper-button-white"></div>
                                  <?php //}?>
                                </div>
                              
                            </div>
                          <?php }else{
							  	
                                echo '<img src="'. $images[0]['sizes'][$size] .'" alt="'. $image['alt'] .'"  />';
                                }?>
                                
                      <?php 
					  endif; ?>
                    </div>
                    
                </div>
                <div class="col-md-3">
            <?php if ($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
            <?php }?>
                    <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
            <?php if ($the_link){ ?>  
            </a>
            <?php }?>
                </div>
              </div>
           <?php endif; // get_row_layout() ==?>
          </div><!-- .blocco -->

		
    <?php  endwhile; ?>
	<?php endif;
	endwhile;
	?>
</div>
<!-- #container -->

<?php get_footer(); ?>

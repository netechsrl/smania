<?php /*
Template Name: Template Login/logout
*/
?>

<?php get_header("page"); ?>
    <style>
        .login-pnl .col-md-6 {
            padding-left: 5px;
            flex-direction: column;
            display: flex;
            min-height: 360px;
            margin: 50px 0;
        }

        .login-pnl .register_page {
            background-color: #f4ece1;
            margin: 50px 0;
            padding: 40px 80px;
        }

        .tml-login, .tml-register {
            background-color: #f4ece1 /*#eae4d1*/;
            width: 100%;
            max-width: 100%;
            padding: 35px;
            flex: 1;
        }

        .tml-login .title-msg, .registration-msg .title-msg {
            font-weight: bold;
            width: 100%;
        }

        .registration-msg {
            background-color: #a49381 /*#9f9283*/;
            width: 100%;
            max-width: 100%;
            padding: 35px;
            color: white;
            height: 100%;
            flex: 1;
        }

        .login-pnl .styled-select {
            border: 1px solid #a49381 /*#9f9283*/;
            height: 44px;
            overflow: hidden;
            width: 100%;
            position: relative;
            margin-bottom: 30px;
        }

        .login-pnl .styled-select input {
            background: transparent none repeat scroll 0 0;
            border: 0 none;
            border-radius: 0;
            color: #a49381 /*#9f9283*/;
            font-size: 15px;
            height: 44px;
            line-height: 1;
            padding: 0 10px;
            width: 120%;
            margin: 0px;
        }

        .login-pnl .tml-submit-wrap #wp-submit {
            background-color: #a49381 /*#9f9283*/;
            color: white;
            width: 440px;
            text-transform: uppercase;
        }

        .login-pnl .recover-msg a {
            font-weight: bold;
        }

        .login-pnl .recover-msg ul li {
            float: none;
            margin: 0 5px;
        }

        .registration-msg .btn-default {
            background: #f4ece1 /*#eae4d1*/ none repeat scroll 0 0;
            color: black;
            text-transform: uppercase;
            width: 200px;
            border-radius: 0px;
            margin: 30px 0;
            padding: 10px;
            text-shadow: none;
        }

        .registration-msg .msg {
            margin-top: 60px;
        }

        .register_page .select-style {
            padding: 0;
            border: 1px solid #a49381;
            height: 44px;
            width: 100%;
            overflow: hidden;
            position: relative;
            margin-bottom: 30px;
            background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/select.png) no-repeat right center;
        }

        .register_page .select-style select {
            opacity: .5;
            color: #a49381;
            font-size: 15px;
            padding: 5px 8px;
            width: 100%;
            border: none;
            box-shadow: none;
            background-color: transparent;
            background-image: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .register_page .select-style select:focus {
            outline: none;
        }

        .register_page textarea {
            border: 1px solid #a49381;
            background: transparent;
            color: #a49381;
            font-size: 15px;
        }

        p.tml-rememberme-wrap {
            margin: 14px 0px !important;
        }
    </style>
<?php while (have_posts()) : the_post(); ?>


    <div class="container login-pnl">
        <?php echo do_shortcode('[theme-my-login]'); ?>
        <?php //the_content(); ?>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>
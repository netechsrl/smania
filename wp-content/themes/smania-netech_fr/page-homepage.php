<?php /*
Template Name: Home page template
*/
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<div class="container">

    <?php 

	// check if the flexible content field has rows of data
if( have_rows('elementi') ):
     // loop through the rows of data
    while ( have_rows('elementi') ) : the_row(); 
		//$the_link = '';
		$image1 = get_sub_field('img1');
		$image2 = get_sub_field('img2');
		$size1 = 'home_vert_big';
		$size2 = 'home_vert_small';
		$size3 = 'home_orizz_big';
		$size4 = 'home_orizz_small';
		$Link_type = get_sub_field('link');
		if( $Link_type == 'Link interno' ):
		$the_link = get_sub_field('link_interno');
		$target = '';
		else:
		$the_link = get_sub_field('link_esterno');
		$target = 'target="_blank"';
		endif;
	?>
		<div class="col-md-6 blocco home clearfix" style="padding:10px 0 0 0;">
        	<!-- modello_1 -->
			<?php if( get_row_layout() == 'modello_1' ): ?>
			<?php if($the_link){ 
			?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
              <?php }?>
                <div class="col-xs-6 ">
                    <div class="txt_container">
                      <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                        <h1><?php the_sub_field('titolo');?></h1>
                        <?php the_sub_field('testo');?>
                        </div>
                    </div>
                    <div class="img_small_down">
                    <?php 
                    $image2 = get_sub_field('img2');
                    echo wp_get_attachment_image( $image2, $size2 ); 
                    ?>
                    </div>
            </div>
                <div class="col-xs-6">
                    <div class="img_big_vertical">
                    <?php 
                    $image1 = get_sub_field('img1');
                    echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                    </div>
                </div>
            <?php if($the_link){ ?>
			</a>
              <?php }?>
			<!-- modello_2 -->
			<?php elseif( get_row_layout() == 'modello_2' ): ?>
			<?php if($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
              <?php }?>
			<div class="col-xs-12">
            	<div class="img_big_orizzontal">
                <?php 
                $image1 = get_sub_field('img1');
                echo wp_get_attachment_image( $image1, $size3 );
				?>
                </div>
			</div>
			<div class="col-xs-12 clearfix" style="margin-top: 10px;">
            	<div class="col-xs-6" style="padding: 0 5px 0 0;">
                  <div class="txt_container">
                    <div class="txt txt3_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo');?>;">
                      <h1><?php the_sub_field('titolo');?></h1>
                      <?php the_sub_field('testo');?>
                      </div>
                  </div>
                </div>
                <div class="col-xs-6" style="padding: 0 0 0 5px;">
                    <div class="img_small_left">
						<?php 
                        $image2 = get_sub_field('img2');
                        echo wp_get_attachment_image( $image2, $size4 ); 
                        ?>
                    </div>
                </div>
    		</div>
            <?php if($the_link){ ?>
			</a>
              <?php }?>
			<!-- modello_3 -->
			<?php elseif( get_row_layout() == 'modello_3' ): ?>
			<?php if($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
              <?php }?>
			<div class="col-xs-6">
            	<div class="img_big_vertical">
                <?php 
                $image1 = get_sub_field('img1');
                echo wp_get_attachment_image( $image1, $size1 );
				?>
                </div>
			</div>
            <div class="col-xs-6">
              <div class="txt_container">
                <div class="txt txt2_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo');?>;">
                  <h1><?php the_sub_field('titolo');?></h1>
                  <?php the_sub_field('testo');?>
                  </div>
              </div>
            </div>
            <?php if($the_link){ ?>
			</a>
              <?php }?>
			<!-- modello_4 -->
			<?php elseif( get_row_layout() == 'modello_4' ): ?>
			<?php if($the_link){ ?>
			<a href="<?php echo $the_link ?>" <?php echo $target  ?>>
              <?php }?>
			<div class="col-xs-12">
            	<div class="img_big_orizzontal">
                <?php 
                $image1 = get_sub_field('img1');
                echo wp_get_attachment_image( $image1, $size3 );
				?>
                </div>
			</div>
			<div class="col-xs-12 clearfix" style="margin-top: 10px;">
              <div class="txt_container">
                <div class="txt txt3_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>;color:<?php the_sub_field('colore_testo');?>;">
                  <h1><?php the_sub_field('titolo');?></h1>
                  <?php the_sub_field('testo');?>
                  </div>
              </div>
            </div>
            <?php if($the_link){ ?>
			</a>
              <?php }?>
           <?php endif; // get_row_layout() ==?>
          </div><!-- .blocco -->

		
    <?php  endwhile; ?>
	<?php endif;
	endwhile;
	?>
</div>
<!-- #container -->

<?php get_footer(); ?>

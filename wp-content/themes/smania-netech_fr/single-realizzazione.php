<?php get_header(); ?>

<div class="container">
    <?php while (have_posts()) {
    the_post(); ?>

    <div class="col-xs-12 blocco realizzazione clearfix">
        <?php
        $details = get_field('elenco');
        foreach ($details as $detail) {
            ?>
            <div class="row">
                <div class="col-md-12 blocco-testo" style="background-color:<?php echo $detail["colore_sfondo"]; ?>;">
                    <div class="txt titolo" style="color:<?php echo $detail["colore_titolo"]; ?>;"> <?php echo $detail["titolo"]; ?> </div>
                    <div class="txt testo" style="color:<?php echo $detail["colore_testo"]; ?>;"> <?php echo $detail["testo"]; ?> </div>
                </div>
                <div class="col-xs-12 blocco-slider">
                    <?php $images = $detail["slider"];
                    if ($images) {
                        ?>
                        <?php if (count($images) > 1) { ?>
                            <div class="blocco_gallery">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php foreach ($images as $image) {
                                            $content = '<div class="swiper-slide">';
                                            $content .= '<img src="'.$image['url'].'" alt="'.$image['title'].'"  />';
                                            $content .= '</div>';
                                            if (function_exists('slb_activate')) {
                                                $content = slb_activate($content);
                                            }
                                            echo $content;
                                        } ?>
                                    </div>

                                    <!--Add Pagination -->
                                    <div class="swiper-pagination"></div>
                                    <!--Add Navigation -->
                                    <div class="swiper-button-prev swiper-button-white"></div>
                                    <div class="swiper-button-next swiper-button-white"></div>
                                    <?php //}?>
                                </div>

                            </div>
                        <?php } else {
                            echo '<img src="'.$images[0]['sizes'][$size2].'" alt="'.$image['alt'].'"  />';
                        } ?>

                        <?php
                    } ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <div style="clear:both;"></div>
</div>
<?php } ?>


<?php get_footer(); ?>



<?php /*
Template Name: Contatti template
*/
?>
<?php get_header("page");

$modulo_contatti = get_field('modulo_contatti');
?>

<div class="container contatti">
    <?php
    while (have_posts()) : the_post();
        ?>
        <!-- form -->
        <div class="row" style="background-color:#f4ece0;    margin-top: 40px;">

            <div class="col-xs-12">
                <?php echo do_shortcode($modulo_contatti); ?>
            </div>

        </div> <!-- / .row - form -->
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <?php echo get_field('colonna_1') ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <?php echo get_field('colonna_2') ?>
            </div>
            <div class="col-md-3 col-sm-6 social">
                <?php echo get_field('colonna_3') ?>
            </div>
            <div class="col-md-2 col-sm-6">
                <?php echo get_field('colonna_4') ?>
            </div>
        </div><!-- / .row -->


        <?php
    endwhile;
    ?>
</div> <!-- #container -->

<?php get_footer(); ?>

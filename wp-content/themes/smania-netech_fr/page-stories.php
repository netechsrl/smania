<?php /*
Template Name: Template Stories
*/
?>


<?php get_header(); ?>			
	
<div id="primary">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<!--<h1><?php the_title(); ?></h1>-->
				<?php the_content(); ?>

				<?php 

				// check for rows (pagine_stories)
				if( have_rows('pagine_stories') ): ?>
					<div id="stories">
					<?php 

					// loop through rows (pagine_stories)
					while( have_rows('pagine_stories') ): the_row(); 
                    $pagina = get_sub_field('seleziona_pagina');
					$layout_row = get_sub_field('seleziona_layout');
					//$cat_text = get_field('descrizione_categoria', $categoria_prod->taxonomy . '_' . $categoria_prod->term_id);
					//$slug = str_replace('valentini-', '', $categoria_prod->slug);
					
						if($layout_row === 'immagine orizzontale'){
							$image = get_sub_field('1_immagine_orizzontale_img');
							$size = 'cat-prod-1-orizz';
							$width = 472;
							$cat_img_dx = '';
						}else if($layout_row === 'immagine verticale'){
							$image = get_sub_field('1_immagine_verticale');
							$size = 'cat-prod-1-vert';
							$width = 274;
							$cat_img_dx = '';
						}else if($layout_row === 'Due Immagini'){
							$image = get_sub_field('2_immagini_destra');
							$size = 'cat-prod-2-b';
							$image2 = get_sub_field('2_immagini_sinistra');
							$size2 = 'cat-prod-2-a';
							$width = 652;
							$cat_img_dx = 'cat_img_dx';
						}
					?>
						<div class="cat_prod_cover clearfix">
                        	<div class="cat_cover_cat" style="max-width:<?php echo $width ?>px">
                        	<a href="<?php echo get_permalink($pagina->ID); ?>">
								<?php if($layout_row === 'Due Immagini'){?>
                                <div class="cat_img_sx">
                                   <?php  echo wp_get_attachment_image( $image2, $size2 );?>
                                   <div class="cat_name"></div>
                                   
                                </div>
                                <?php }?>
                                <div class="<?php echo $cat_img_dx ?>">
                                   <?php  echo wp_get_attachment_image( $image, $size );?>
                                   <div class="cat_name">
                                        <?php echo $pagina->post_title; ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/freccia01.png" />
                                   </div>
                                </div>
                            </a>
                            </div>
						</div>	

					<?php endwhile; // while( have_rows('pagine_stories') ): ?>
					</div>
				<?php endif; // if( ghave_rows('pagine_stories') ): ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
</div><!-- #primary -->
		
<?php get_footer(); ?>
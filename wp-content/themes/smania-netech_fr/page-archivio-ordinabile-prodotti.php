<?php /*
Template Name: Prodotti Archivio ordinabile
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) :?>
<?php while(have_posts()) : the_post(); ?>

<div id="tax-description" class="col-md-8 col-md-offset-4" style="padding: 0 0 60px 23px;">
  <h1>
    <?php the_title(); ?>
  </h1>
  <?php the_content(); ?>
</div>
<div class="clearfix"></div>
<div class="cont-grid" >
  <div class="grid"> 
    <!--<div class="row">	-->
    <?php   
			
			$posts = get_field('prodotto_ord');
			if( $posts ):
			//var_dump($posts);
			foreach( $posts as $p ):
			
			?>
    <div class="item">
      <div class="cat_prod_cover clearfix" style="padding:0;"> <a href="<?php echo get_permalink( $p->ID ); ?>">
        <div > <?php echo get_the_post_thumbnail($p->ID, 'masonry-grid', array('class' => 'grid-cop')); ?>
          <div class="cat_name "> <?php echo get_the_title( $p->ID ); ?> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/freccia01.png" /> </div>
        </div>
        </a> </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    
    <!--</div>--> 
  </div>
</div>
<?php endwhile; // have_posts()) :?>
<?php else : ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; // have_posts()) :?>
<?php get_footer(); ?>

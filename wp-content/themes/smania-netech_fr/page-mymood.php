<?php /*
Template Name: myMood template
*/
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<div class="container">

    <?php 

//**** Testatina *****//
if( have_rows('testatina') ):
     // loop through the rows of data
    while ( have_rows('testatina') ) : the_row(); ?>
	
		<div class="col-xs-12 blocco mymood clearfix" >
            <div class="col-xs-12">
              <div  style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>; text-align:center; padding:20px 0; margin-top:10px;">
                  <h1><?php the_sub_field('titolo');?></h1>
                  <?php 
                  if(the_sub_field('altro_testo')){
                    the_sub_field('altro_testo');
                  }
                  ?>
              </div>
            </div>
        </div>
<?php
	endwhile;
endif;

	// check if the flexible content field has rows of data
if( have_rows('elementi') ):
     // loop through the rows of data
    while ( have_rows('elementi') ) : the_row(); 
		$image1 = get_sub_field('img1');
		$image2 = get_sub_field('img2');
		$size1 = 'pensiero_small';
		$size2 = 'pensiero_big';
		$size3 = 'pensiero_quad';
		$Link_type = get_sub_field('link');
		if( $Link_type === 'Link interno' ):
		$the_link = get_sub_field('link_interno');
		$target = '';
		else:
		$the_link = get_sub_field('link_esterno');
		$target = ' target="_blank"';
		endif;
	?>
		<div class="col-xs-12 blocco mymood clearfix" >
        	<!-- modello_1 -->
			<?php if( get_row_layout() == 'modello_1' ): ?>
              <div class="row">
                <div class="col-md-6">
                    <div class="txt txt1_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
                    <div class="img_small_down">
                    <?php 
                    $image1 = get_sub_field('img1');
                    echo wp_get_attachment_image( $image1, $size1 );
                    ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div >
                    <?php 
                    echo wp_get_attachment_image( $image2, $size3 );
                    ?>
                    </div>
                </div>
              </div>
			<!-- modello_2 -->
			<?php elseif( get_row_layout() == 'modello_2' ): ?>
              <div class="row">
                <div class="col-md-9">
                    <?php 
                    $image1 = get_sub_field('img1');
                    echo wp_get_attachment_image( $image1, $size2 );
                    ?>
                </div>
                <div class="col-md-3">
                    <div class="txt txt2_h" style="background-color:<?php the_sub_field('colore_sfondo'); ?>; color:<?php the_sub_field('colore_testo');?>;">
                    <h1><?php the_sub_field('titolo');?></h1>
                    <?php the_sub_field('testo');?>
                    </div>
                </div>
              </div>
           <?php endif; // get_row_layout() ==?>
          </div><!-- .blocco -->

		
    <?php  endwhile; ?>
	<?php endif;
	endwhile;
	?>
</div>
<!-- #container -->

<?php get_footer(); ?>

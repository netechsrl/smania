jQuery(function () {

// acf.add_action('ready', function ($el) {
    acf.add_action('load', function ($el) {
        // $el will be equivalent to $('body')
        console.log("caricato tutto");
    });

    acf.add_action('append', function ($newElementRow) {
        // $newElementRow will be equivalent to the new element being appended $('tr.row')

        console.log("aggiunto elemento");

        jQuery($newElementRow).find('.acf-field[data-name=materiale]').on("select2-selecting", function ($materialeCombo) {
            var materialeFilter = "[" + jQuery($materialeCombo).select2('data')[0].choice.text.toUpperCase() + "]";
            var hiddenField = jQuery($newElementRow).find('.acf-field[data-name=finiture] div.acf-relationship div.acf-hidden').find('input');

            jQuery($newElementRow).find('div.values ul.list').empty();

            jQuery($newElementRow).find('div.choices ul.list li').each(function (idx, $materialiListLi) {
                var data_id = jQuery($materialiListLi).find('span.acf-rel-item').attr("data-id");
                var materialeSelector = jQuery($materialiListLi).find('span.acf-rel-item span').html();
                var $materialeHiddenField = hiddenField.clone();

                $materialeHiddenField.attr('name', $materialeHiddenField.attr('name') + '[]');
                $materialeHiddenField.val(data_id);

                if (materialeFilter != materialeSelector) {
                    jQuery($materialiListLi).addClass("materialeHidden");
                } else {
                    jQuery($materialiListLi).removeClass("materialeHidden");
                    var $newLiNode = jQuery($materialiListLi).clone();
                    $newLiNode.append($materialeHiddenField);
                    $newLiNode.find('span').append('<a href="#" class="acf-icon -minus small dark" data-name="remove_item"></a>');
                    jQuery($newElementRow).find('div.values ul.list').append($newLiNode);
                    jQuery($materialiListLi).find('span.acf-rel-item').addClass("disabled");
                }

            });

        });

    });

});
<?php get_header('page'); ?>

<div class="container ">
  <div id="primary">
    <div id="content" role="main">
      <?php $i = 0; ?>
      <?php while ( have_posts() ) : the_post(); 
  		//$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  ?>
      <?php if($i % 3 == 0) { ?>
      <div class="row" style="padding: 0;">
        <?php }?>
        <a href="<?php the_permalink() ?>">
        <div class="col-sm-4 item-grid ispirazioni" style="padding:0 5px 5px 0;">
          <?php the_post_thumbnail('ispirazioni_thumb') ?>
          <h1 class="grid-tit">
            <?php the_title();?>
          </h1>
          <div class="filter"></div>
        </div>
        </a>
        <?php $i++;
  if($i != 0 && $i % 3 == 0) { ?>
      </div>
      <div class="clearfix"></div>
      <?php }
  
  ?>
      <?php endwhile; // end of the loop. ?>
    </div>
  </div>
  <!-- #content -->
</div>
<!-- #primary -->
</div>
<!-- / .container -->
<?php get_footer(); ?>

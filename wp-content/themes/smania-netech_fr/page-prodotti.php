<?php /*
Template Name: Prodotti template
*/
?>
<?php get_header('page'); ?>


<?php

$page_id = get_the_id();
$title   = get_the_title($page_id);

if ( ! $post->post_parent > 0) {
    $args = array(
        'post_type'      => 'prodotto',
        'orderby'        => 'date',
        'order'          => 'DESC',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
    );

} else {
    $args = array(
        'post_type'      => 'prodotto',
        'orderby'        => 'date',
        'order'          => 'DESC',
        'post_status'    => 'publish',
        'tax_query'      => array(
            array(
                'taxonomy' => 'categoria-prodotto',
                'field'    => 'name',
                'terms'    => $title,
            ),
        ),
        'posts_per_page' => -1,
    );
}
$prod = new WP_Query($args);

?>

<div class="container">
    <div id="primary" class="col-sm-9 prodotti-list">
        <div class="container-fluid">
            <?php $i = 0; ?>
            <?php while ($prod->have_posts()) : $prod->the_post();

                $filtersClass = "";

                $ambiente = get_the_terms($prod->ID, 'ambiente');
                if ($ambiente && ! is_wp_error($ambiente)) :
                    foreach ($ambiente as $amb) {
                        $filtersClass .= " ".$amb->slug;
                    }
                endif;

                $collezione = get_the_terms($prod->ID, 'collezione');
                if ($collezione && ! is_wp_error($collezione)) :
                    foreach ($collezione as $coll) {
                        $filtersClass .= " ".$coll->slug;
                    }
                endif;

                $arredi = get_the_terms($prod->ID, 'arredo');
                if ($arredi && ! is_wp_error($arredi)) :
                    foreach ($arredi as $arredo) {
                        $filtersClass .= " ".$arredo->slug;
                    }
                endif;

                ?>
                <div class="col-md-3 item-grid <?php echo $filtersClass; ?>">
                    <a href="<?php the_permalink() ?>">
                        <div>
                            <?php the_post_thumbnail('prodotto') ?>
                            <p class="grid-tit">
                                <?php the_title(); ?>
                            </p>
                        </div>
                    </a>
                </div>
            <?php endwhile; // end of the loop. ?>
        </div>
    </div>
    <!-- #content -->
    <div class="col-sm-3" id="prod_sidebar">
        <?php get_sidebar('prod_filter'); ?>
    </div>
</div>
<!-- #primary -->
</div>
<!-- / .container -->
<?php get_footer(); ?>

<script type="text/javascript">
    if (!String.prototype.trim) {
        (function () {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function () {
                return this.replace(rtrim, '');
            };
        })();
    }

    $(document).ready(function () {

        $container = $('.prodotti-list .container-fluid');

        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        var filtriAttivi = [];
        $('.isotofilter').change(function () {
            filtriAttivi = [];

            $.each($('.filter-pnl'), function (idx, el) {
                var pnl = $(el);
                var groupName = pnl.attr('id');
                var groupFilter = [];


                $.each($('#' + pnl.attr('id') + ' .isotofilter'), function (idx, el) {
                    if ($(this).is(":checked")) {
                        groupFilter.push($(el).attr('data-filter'));
                    }
                });

                if (groupFilter.length > 0) {
                    filtriAttivi[groupName] = groupFilter;
                }
            });

            reorder();
        });

        $('#amb a').click(function () {
            var amb = $(this).attr('data-filter');
            $('#amb').attr('data-active', amb);
            reorder();
            return false;
        });

        $('#coll a').click(function () {
            var coll = $(this).attr('data-filter');
            $('#coll').attr('data-active', coll);
            reorder();
            return false;
        });

        $('#arr a').click(function () {
            var arr = $(this).attr('data-filter');
            $('#arr').attr('data-active', arr);
            reorder();
            return false;
        });

        function reorder() {

            var filterFn = function (index) {
                var $this = $(this);
                var result1 = false;
                var finalResult = null;
                for (var key in filtriAttivi) { // ambiente arredo
                    result1 = false;
                    for (var n = 0; n < filtriAttivi[key].length; n++) { //ufficio soggiorno
                        if ($this.attr('class').indexOf(filtriAttivi[key][n].replace('.', '')) !== -1) {
                            result1 = ((result1 == null) ? true : (result1 || true));
                        }
                    }
                    finalResult = ((finalResult == null) ? result1 : (finalResult && result1));
                }
                return finalResult;
            };

            $container.isotope({
                filter: ((Object.keys(filtriAttivi).length == 0) ? "*" : filterFn),
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

        }
    });
</script>
<?php get_header("page");

if (current_user_can('read')) { ?>

    <div id="primary" class="container area-ris">
        <div id="content" role="main">
        	<div class="welcome_txt">
            	<?php echo get_field('testo1', 'option' ) ?>
            </div>
        	<p style="font-weight:bold; color: #a46e24;"><?php echo __('Seleziona le macroaree per visualizzare tutti i relativi contenuti scaricabili') ?></p>

            <?php
            $myterms = get_terms(array(
                'taxonomy'   => 'cat-ar',
                'parent'     => 0,
                'orderby'    => id,
                'hide_empty' => false,
                'order'      => 'asc',
            ));
            ?>
            <ul class="nav nav-pills">
                <?php
                $active = "class=\"active\"";
                foreach ($myterms as $myterm) {
                    ?>
                    <li <?php echo $active; ?>>
                        <a href="#<?php echo $myterm->slug ?>" data-toggle="tab"><?php echo $myterm->name ?></a>
                    </li>
                    <?php
                    $active = "";
                }
                ?>
            </ul>
            <div class="tab-content clearfix">
                <?php
                $active = "active";
                foreach ($myterms as $myterm) {
                    ?>
                    <div class="tab-pane <?php echo $active; ?>" id="<?php echo $myterm->slug ?>">
                        <?php
                        // prendo i post della tassonomia corrente
                        $curr_taxonomy_posts = array();
                        while (have_posts()) {
                            the_post();
                            $product_terms = wp_get_object_terms($post->ID, 'cat-ar', array('parent' => 0));
                            if ($myterm->name == $product_terms[0]->name) {
                                $curr_taxonomy_posts[] = $post;
                            }
                        }
                        //-- prendo i post della tassonomia corrente
                        foreach ($curr_taxonomy_posts as $currPost) {
                            ?>


                            <div class="panel-group" id="accordion-<?php echo $currPost->ID; ?>" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="<?php /*?>panel-heading<?php */?>" role="tab" id="heading-<?php echo $currPost->ID; ?>"
                                         style="background-color:transparent;">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse"
                                               data-parent="#accordion-<?php echo $currPost->ID; ?>"
                                               href="#collapse-<?php echo $currPost->ID; ?>" aria-expanded="true"
                                               aria-controls="collapseOne">
                                                <h5>
                                                    <?php echo get_the_title($currPost->ID); ?>
                                                </h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapse-<?php echo $currPost->ID; ?>" class="panel-collapse collapse"
                                         role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <?php
                                            $files = get_field('elenco_file');
                                            foreach ($files as $file) {
                                                ?>
                                                <div class="col-sm-9" style="padding: 30px 0 0 20px;">
                                                    <span class="col-xs-2" style="font-weight:bold; margin-right:40px;"><?php echo $file['nome_allegato']; ?></span>
                                                    <span class="col-xs-2" style="margin-right:40px;"><?php echo $file['data_allegato']; ?></span>
                                                    <span class="col-xs-2" style="margin-right:40px;"><?php echo $file['info_allegato']; ?></span>
                                                </div>
                                                <div class="col-sm-3" style="padding: 30px 0 0 20px;">
                                                	<a class="download-btn" href="<?php echo $file['allegato']; ?>"><?php echo __('Download') ?></a>
                                                </div>

                                                <div style="clear:both;"></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <?php }
                        ?>
                    </div>
                    <?php
                    $active = "";
                }
                ?>
            </div>

        </div>   <!-- #content -->
    </div> <!-- #primary -->
<?php } else {
    echo "<script language='javascript'>\n";
    echo "  location.href=\"".home_url('/login')."\"";
    echo "</script>\n";
}

get_footer(); ?>

<script type="text/javascript">
    $('.menu_download a.logic-btn').click(function () {
        var $this = $(this);
        $('.menu_download a.logic-btn').removeClass("active");
        $this.addClass("active");
        $('div.section').hide("slow", function () {
            $('div#' + $this.attr('data-id')).show("slow");
        });
    });
	
	    $('.panel-title a').click(function () {
        var $this = $(this);
        //$('.panel-title a').removeClass("active");
        $this.toggleClass("active");
        
    });

	
</script>
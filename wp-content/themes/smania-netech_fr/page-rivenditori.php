<?php /*
Template Name: Rivenditori template
*/
?>
<?php get_header('rivenditori'); ?>

<?php
$prod = new WP_Query(array(
    'post_type'   => 'rivenditore',
    'orderby'     => 'title',
    'order'       => 'ASC',
    'post_status' => 'publish',
));

$countries = array();
while ($prod->have_posts()) {
    $prod->the_post();

    $tmpcountry_code = strtolower(preg_replace('/\s+/', '', get_field('nazione')));
    $tmpcountry      = get_field('nazione');
    $tmpcity_code    = strtolower(preg_replace('/\s+/', '', get_field('citta')));
    $tmpcity         = get_field('citta');

    if ( ! array_key_exists($tmpcountry_code, $countries)) {
        $countries[$tmpcountry_code]["label"]                 = $tmpcountry;
        $countries[$tmpcountry_code]["cities"][$tmpcity_code] = $tmpcity;
    } else {
        if ( ! array_key_exists($tmpcity_code, $countries[$tmpcountry_code]["cities"])) {
            $countries[$tmpcountry_code]["cities"][$tmpcity_code] = $tmpcity;
        }
    }

}
?>
<script>
    jQuery(document).ready(function () {
        jQuery('#filter-by-country').on('change', function () {
            jQuery('#filter-by-city').val(-1);

            jQuery.each(jQuery('#filter-by-city').children('option'), function (i, option) {
                if (jQuery(option).val() != "" && jQuery(option).val() != "-1") {
                    jQuery(option).addClass('no-display');
                }
            });

            jQuery.each(jQuery('#filter-by-city').children('option'), function (i, option) {
                if (jQuery(option).hasClass(jQuery('#filter-by-country').val())) {
                    jQuery(option).removeClass('no-display');
                }
            });
        });

        var $grid_cases = jQuery('.rivenditori-list .container').isotope({});
        jQuery('#filterRivenditori .btn-default').on('click', function () {
            var filterValue = (jQuery('#filter-by-city').val() != null && jQuery('#filter-by-city').val() != "-1" ) ? jQuery('#filter-by-city').val() : jQuery('#filter-by-country').val();
            console.log(filterValue);
            $grid_cases.isotope({filter: filterValue});

            SZPlaces.removeAllMArkers();
            SZPlaces.init();
            SZPlaces.initMap2();
        });

    });
</script>
<div class="container body">
    <div class="row">
        <div class="col-md-6 filter-pnl">
            <div class="container">
                <form action="" name="filterRivenditori" id="filterRivenditori" method="get">
                    <div class="styled-select">
                        <select id="filter-by-country" name="cntr">
                            <option value="-1" disabled selected>STATO</option>
                            <option style="color:black;" value="">-- Tutti --</option>
                            <?php foreach ($countries as $key => $value) { ?>
                                <option style="color:black;"
                                        value=".<?php echo $key; ?>"><?php echo $value["label"]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="styled-select">
                        <select id="filter-by-city" name="brd">
                            <option value="-1" disabled selected>CITTA'</option>
                            <?php foreach ($countries as $key => $value) {
                                foreach ($value["cities"] as $key1 => $value1) { ?>
                                    <option class=".<?php echo $key; ?> no-display" style="color:black; "
                                            value=".<?php echo $key1; ?>"><?php echo $value1; ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div>
                        <div class="btn btn-default">Cerca rivenditori</div>
                    </div>
                </form>
            </div>
            <div class="rivenditori">
            <?php while ( have_posts() ) : the_post(); ?>
					   <?php $images = get_field('gallery');
                        if( $images ):  
						
						?>
                        	<?php if (count($images) >1){?>
                            <div >
                                <div class="swiper-container">
                                  <div class="swiper-wrapper">
                                    <?php foreach( $images as $image ): 
                                            $content = '<div class="swiper-slide">';
                                                //$content .= '<a class="gallery_image nivo" data-lightbox-gallery="gallery1" href="'. $image['url'] .'">';
                                                     $content .= '<img src="'. $image['sizes']['cover_thumb'] .'" alt="'. $image['alt'] .'"  />';
                                                //$content .= '</a>';
                                            $content .= '</div>';
                                            if ( function_exists('slb_activate') ){
                                            $content = slb_activate($content);
                                            }
                                            echo $content;
                                            endforeach; ?>
                                  </div>
                                 
                                  <!--Add Pagination --> 
                            		<div class="swiper-pagination"></div>
                                  <!--Add Navigation -->
                                  <div class="swiper-button-prev swiper-button-white"></div>
                                  <div class="swiper-button-next swiper-button-white"></div>
                                  <?php //}?>
                                </div>
                              
                            </div>
                          <?php }else{
							  	
                                echo '<img src="'. $images[0]['sizes']['cover_thumb'] .'" alt="'. $image['alt'] .'"  />';
                                }?>
                                
                      <?php 
					  
					  endif; 
					  
					  endwhile;
					  ?>
                   
        
        
      		</div>
        </div>
        <div class="col-md-6 rivenditori-list">
            <div class="container">
                <?php while ($prod->have_posts()) {
                    $prod->the_post();

                    $nome         = get_field('nome');
                    $image        = get_field('marker');
                    $pos          = get_field('latlng');
                    $city         = get_field('citta');
                    $city_code    = strtolower(preg_replace('/\s+/', '', get_field('citta')));
                    $country      = get_field('nazione');
                    $country_code = strtolower(preg_replace('/\s+/', '', get_field('nazione')));
                    ?>
                    <div class="<?php echo $country_code; ?> <?php echo $city_code; ?> row rivenditore"
                         data-img="<?php echo $image['url']; ?>" data-title="<?php echo $nome; ?>"
                         data-latlng="<?php echo $pos; ?>">
                        <div class="col-xs-1 col-md-offset-1 marker">
                            <?php if ($image) { ?>
                                <div class="gmaps_marker">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                                </div>
                            <?php } ?>

                        </div>
                        <div class="col-xs-9 info">
                            <div class="nome_riv"><?php echo $nome; ?></div>
                            <div class="indirizzo_riv"><?php echo get_field('indirizzo'); ?></div>
                            <div class="indirizzo2_riv"><?php echo get_field('cap')." - ".get_field('citta')." (".get_field('provincia').") - ".get_field('nazione'); ?></div>
                            <div class="telefono_riv"><?php echo get_field('telefono'); ?></div>
                            <div class="email_riv"><?php echo get_field('email'); ?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

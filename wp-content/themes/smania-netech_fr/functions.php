<?php // Add prefix ('smania-') to slug term (taxonomy = "categoria-prodotto")

/*add_action('created_term', 'my_add_prefix_to_term', 10, 3);
function my_add_prefix_to_term( $term_id,$tt_id,$taxonomy ) {
    if( $taxonomy == 'categoria-prodotto'){
        $term = get_term( $term_id, $taxonomy );
        $args = array('slug'=>'smania-'.$term->slug);
        wp_update_term( $term_id,$taxonomy, $args );
    }
}*/

?>

<?php // CONTENT limit excerpt number of words

function custom_excerpt_length($length)
{
    return 10;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);
?>

<?php // REPLY comment script 

function fullby_enqueue_comments_reply()
{
    if (get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('comment_form_before', 'fullby_enqueue_comments_reply');

?>
<?php // MENU 

function register_my_menus()
{
    register_nav_menus(array(
        'primary'   => __('Primary navigation'),
        'secondary' => __('Secondary navigation'),
        'mobile'    => __('Mobile navigation'),
        'footer'    => __('Footer navigation'),
        'prodotti'  => __('Prodotti navigation'),
    ));
}

add_action('init', 'register_my_menus');

/*add_filter( 'nav_menu_link_attributes', 'wpse121123_contact_menu_atts', 10, 3 );
function wpse121123_contact_menu_atts( $atts, $item, $args )
{
  // The ID of the target menu item
  $menu_target = 11;

  // inspect $item
  if ($item->ID == $menu_target) {
    $atts['data-toggle'] = 'dropdown';
    $atts['class'] = 'data-toggle';
    $atts['id'] = 'search-form';
  }
  return $atts;
}
*/

?>
<?php // BOOTSTRAP MENU - Custom navigation walker (Required)

require_once('wp_bootstrap_navwalker.php');
require_once('yamm-nav-walker.php');

?>
<?php // CUSTOM THUMBNAIL 

add_theme_support('post-thumbnails');

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

if (function_exists('add_image_size')) {
    add_image_size('quad', 400, 400, true); //(cropped)
    add_image_size('single', 800, 494, true); //(cropped)
    add_image_size('logo', 9999, 65); //(box-resizing)
    add_image_size('cover', 1600, 560, true); //(cropped)
    add_image_size('cover_thumb', 570, 200, true); //(cropped)
    add_image_size('home_vert_big', 280, 510, true); //(cropped)
    add_image_size('home_vert_small', 280, 320, true); //(cropped)
    add_image_size('home_orizz_big', 566, 318, true); //(cropped)
    add_image_size('home_orizz_small', 280, 182, true); //(cropped)
    add_image_size('prodotto', 460, 9999); //(box-resizing)
    add_image_size('prod_gallery', 300, 184, true); //(cropped)
    add_image_size('azienda_small', 280, 185, true); //(cropped)
    add_image_size('azienda_big', 1140, 320, true); //(cropped)
    add_image_size('pensiero_big', 850, 310, true); //(cropped)
    add_image_size('pensiero_small', 564, 315, true); //(cropped)
    add_image_size('pensiero_quad', 566, 510, true); //(cropped)
    add_image_size('mymood_small', 564, 178, true); //(cropped)
    add_image_size('ispirazioni_thumb', 380, 246, true); //(cropped)
    add_image_size('ispirazioni_big', 1140, 9999); //(box-resizing)
    add_image_size('realizzazioni_big', 850, 510, true); //(cropped)
}

?>

<?php // WIDGET SIDEBAR 

/*if ( function_exists('register_sidebar') )
    register_sidebar(array('name'=>'Primary Sidebar',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
register_sidebar(array('name'=>'Secondary Sidebar',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));*/

?>

<?php // EXLUDE CUSTOM POST TYPE FROM SEARCH
add_action('init', 'update_my_custom_type', 99);

function update_my_custom_type()
{
    global $wp_post_types;

    if (post_type_exists('cover-slider')) {

        // exclude from search results
        $wp_post_types['cover-slider']->exclude_from_search = true;
    }
}

?>

<?php // THEME OPTIONS

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false,
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Header Settings',
        'menu_title'  => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Footer Settings',
        'menu_title'  => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));
	
	acf_add_options_sub_page(array(
        'page_title'  => 'Theme Area Riservata Settings',
        'menu_title'  => 'Area Riservata',
        'parent_slug' => 'theme-general-settings',
    ));

}
?>
<?php // BREADCRUMBS (Yoast) custom link

add_filter('wpseo_breadcrumb_output', 'custom_wpseo_breadcrumb_output');

function custom_wpseo_breadcrumb_output($output)
{
    if (is_singular('prodotto')) {
        $from   = array('categoria-prodotto', 'prodotto');
        $to     = __('products');
        $output = str_replace($from, $to, $output);
    }

    return $output;
}

?>

<?php
/**
 * Retrieves the navigation to next/previous post, when applicable.
 *
 * @since 4.1.0
 * @since 4.4.0 Introduced the `in_same_term`, `excluded_terms`, and `taxonomy` arguments.
 *
 * @param array $args {
 *     Optional. Default post navigation arguments. Default empty array.
 *
 * @type string $prev_text Anchor text to display in the previous post link. Default '%title'.
 * @type string $next_text Anchor text to display in the next post link. Default '%title'.
 * @type bool $in_same_term Whether link should be in a same taxonomy term. Default false.
 * @type array|string $excluded_terms Array or comma-separated list of excluded term IDs. Default empty.
 * @type string $taxonomy Taxonomy, if `$in_same_term` is true. Default 'category'.
 * @type string $screen_reader_text Screen reader text for nav element. Default 'Post navigation'.
 * }
 * @return string Markup for post links.
 */

function my_get_the_post_navigation($args = array())
{
    $args = wp_parse_args($args, array(
        'prev_text'          => '%title',
        'next_text'          => '%title',
        'in_same_term'       => false,
        'excluded_terms'     => '',
        'taxonomy'           => 'category',
        'screen_reader_text' => __(' '),
    ));

    $navigation = '';

    $previous = get_previous_post_link('<div class="nav-previous">%link</div>', $args['prev_text'],
        $args['in_same_term'], $args['excluded_terms'], $args['taxonomy']);

    $next = get_next_post_link('<div class="nav-next">%link</div>', $args['next_text'], $args['in_same_term'],
        $args['excluded_terms'], $args['taxonomy']);

    // Only add markup if there's somewhere to navigate to.
    if ($previous || $next) {
        $navigation = _navigation_markup($previous.$next, 'post-navigation', $args['screen_reader_text']);
    }

    return $navigation;
}

/**
 * Displays the navigation to next/previous post, when applicable.
 *
 * @since 4.1.0
 *
 * @param array $args Optional. See get_the_post_navigation() for available arguments.
 *                    Default empty array.
 */
function my_the_post_navigation($args = array())
{
    echo my_get_the_post_navigation($args);
}

/**
 * Costum fields in search: Aggiungo la possibilità di ricercare anche i custom fields
 * Extend WordPress search to include custom fields
 *
 * http://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join($join)
{
    global $wpdb;

    if (is_search()) {
        $join .= ' LEFT JOIN '.$wpdb->postmeta.' ON '.$wpdb->posts.'.ID = '.$wpdb->postmeta.'.post_id ';
    }

    return $join;
}

add_filter('posts_join', 'cf_search_join');

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where($where)
{
    global $wpdb;

    if (is_search()) {
        $where = preg_replace("/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where);
    }

    return $where;
}

add_filter('posts_where', 'cf_search_where');

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct($where)
{
    global $wpdb;

    if (is_search()) {
        return "DISTINCT";
    }

    return $where;
}

add_filter('posts_distinct', 'cf_search_distinct');
/**
 * Costum fields in search: end
 **/

/** Move Yoast Seo metabox to bottom **/
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

/** aggiungo elemento a ACF relationship **/
function my_relationship_result($title, $post, $field, $post_id)
{

    // load a custom field from this $object and show it in the $result
    $tipo      = get_field('tipo_finitura', $post->ID);
    $materiale = get_field('materiale', $post->ID);

    // append to title

    $titolo = '<span style="opacity:.6;">['.strtoupper(get_the_title($materiale->ID)).']</span> '.$title.' '.$tipo;
    $title  = $titolo;

    // return
    return $title;

}

add_filter('acf/fields/relationship/result/name=finiture', 'my_relationship_result', 10, 4);
//add_filter('acf/fields/relationship/result/name=sel_finiture', 'my_relationship_result', 10, 4);

/** aggiungo elemento a ACF relationship **/
function my_relationship2_result($title, $post, $field, $post_id)
{

    // load a custom field from this $object and show it in the $result
    $col_finitura = get_field('sel_finiture', $post->ID);
    $finitura     = get_field('materiale', $post->ID);
    if ($col_finitura) {
        $tit  = get_the_title($col_finitura->ID);
        $tipo = get_field('tipo_finitura', $col_finitura->ID);
    } else if ($finitura) {
        $tit  = get_the_title($finitura->ID);
        $tipo = get_field('tipo_finitura', $post->ID);
        //$tipo = '';
    }

    // append to title

    $titolo = '<span style="opacity:.6;">['.strtoupper($tit).' '.$tipo.']</span> '.$title;
    $title  = $titolo;

    // return
    return $title;

}

add_filter('acf/fields/relationship/result/name=finiture_ispirazioni', 'my_relationship2_result', 10, 4);

/** aggiungo elemento a ACF post object **/
function my_post_object_result($title, $post, $field, $post_id)
{

    // load a custom field from this $object and show it in the $result
    $tipo      = get_field('tipo_finitura', $post->ID);
    $materiale = get_field('materiale', $post->ID);

    // append to title

    $titolo = '<span style="opacity:.6;">['.strtoupper(get_the_title($materiale->ID)).']</span> '.$title.' '.$tipo;
    $title  = $titolo;

    // return
    return $title;

}

add_filter('acf/fields/post_object/result/name=sel_finiture', 'my_post_object_result', 10, 4);

///** aggiungo il materiale al nome della finitura **/
//function add_custom_title( $data, $postarr ) {
//  if($data['post_type'] == 'finiture') {
//	$materiale = get_field('materiale', $post->ID);
//	$mat= strtoupper(get_the_title($materiale->ID)).' | ';
//	$data['post_title'] = str_replace($mat,'', $data['post_title']);
//	//if (strpos($data['post_title'], $mat) !== false) {
//    if($materiale) {
//      $data['post_title'] = $mat.$data['post_title'];
//	  $mat = '';
//	//}
//    }
//  }
//  return $data;
//}
//add_filter('wp_insert_post_data', 'add_custom_title', 10, 2 );

// COLONNE in Custom Post ' Colori'
/***
 * Aggiungo la colonne "Finitura" sezione wp-admin di Colori Finitura
 **/

function add_colore_finitura_columns($columns)
{
    $columns['finitura'] = 'Finitura';
    //add more columns as needed

    // as with all filters, we need to return the passed content/variable
    return $columns;
}

add_filter('manage_colore-finitura_posts_columns', 'add_colore_finitura_columns');

function custom_colore_finitura_column($column)
{
    global $post;

    switch ($column) {

        case 'finitura':
            $finitura = get_field('sel_finiture');
            $tipo     = get_field('tipo_finitura', $finitura->ID);

            //var_dump($finitura);
            if ($finitura) {

                $post = $finitura;
                setup_postdata($post);
                $tit = get_the_title().' '.$tipo;

                wp_reset_postdata();
            }
            echo $tit;
            break;

    }
}

add_action('manage_colore-finitura_posts_custom_column', 'custom_colore_finitura_column');

add_filter('manage_edit-colore-finitura_sortable_columns', 'my_finitura_sortable_columns');

function my_finitura_sortable_columns($columns)
{

    $columns['finitura'] = 'finitura';

    //$columns['marchio'] = 'marchio';

    return $columns;
}

add_filter('manage_edit-colore-finitura_sortable_columns', 'finitura_sorting');
function finitura_sorting($columns)
{
    $columns['finitura'] = array('finitura', 1);

    //$columns['marchio'] = array('brand');

    return $columns;
}

add_filter('request', 'finitura_orderby');
function finitura_orderby($vars)
{

    if (isset($vars['orderby']) && 'finitura' == $vars['orderby']) {
        $vars = array_merge($vars, array(
            'meta_key' => 'sel_finiture',
            'orderby'  => 'meta_value',
        ));
    }

    return $vars;

}

// POPULAR POST

if ( ! function_exists('wpb_set_post_views')) {

    function wpb_set_post_views($postID)
    {
        $count_key = 'wpb_post_views_count';
        $count     = get_post_meta($postID, $count_key, true);
        if ($count == '') {
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        } else {
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

/* add post views to single page */
if ( ! function_exists('wpb_track_post_views')) {

    function wpb_track_post_views($post_id)
    {
        if ( ! is_single()) {
            return;
        }
        if (empty ($post_id)) {
            global $post;
            $post_id = $post->ID;
        }
        wpb_set_post_views($post_id);
    }
}

add_action('wp_head', 'wpb_track_post_views');

function my_enqueue($hook)
{
    if ('post.php' != $hook && 'post-new.php' != $hook) {
        return;
    }
    wp_enqueue_script('admin_custom_post_1', get_stylesheet_directory_uri().'/js/admin-custom.js');
    wp_enqueue_style('admin_custom_post_1', get_stylesheet_directory_uri().'/css/admin-custom.css');
}

add_action('admin_enqueue_scripts', 'my_enqueue', 999);

function my_relationship_query($args, $field, $post_id)
{
    $args['paged'] = 0;
    $args['posts_per_page'] = 999;

    return $args;
}

// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function recursiveItems($post, $level)
{
    //var_dump($level);
    ?>
    <?php
    $riga_sep = 'style="border-top:1px solid #000;"';
    if ($level == 0) {
        $pos_file = 'style="padding: 30px 0 0 20px;"';
    } else {
        $pos_file = 'class="col-sm-6 col-sm-offset-6"';
    }
    ?>

    <div class="panel-group" id="accordion-<?php echo $post->ID; ?>" role="tablist"
         aria-multiselectable="true" <?php echo $riga_sep ?>>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading-<?php echo $post->ID; ?>"
                 style="background-color:transparent;">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-<?php echo $post->ID; ?>"
                       href="#collapse-<?php echo $post->ID; ?>" aria-expanded="true"
                       aria-controls="collapseOne">
                        <?php if ($level == 0) { ?>
                            <h1><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon_dir.png">
                                <?php echo get_the_title($post->ID); ?>
                            </h1>
                        <?php } else { ?>
                            <div class="col-sm-3">
                                <h1><img style="margin-left:-15px"
                                         src="<?php bloginfo('stylesheet_directory'); ?>/img/freccia01.png"></h1>
                            </div>
                            <div class="col-sm-9">
                                <h1><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon_dir.png">
                                    <?php echo get_the_title($post->ID); ?>
                                </h1>
                            </div>

                        <?php } ?>
                    </a>
                </h4>
            </div>
            <div id="collapse-<?php echo $post->ID; ?>" class="panel-collapse collapse"
                 role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <?php
                    $level++;

                    $childs_post = get_children(array('post_parent' => $post->ID, 'post_type' => 'area-riservata'));

                    // ciclo sui post
                    foreach ($childs_post as $curr) {
                        recursiveItems($curr, $level);
                    }

                    if (have_rows('seleziona_file', $post->ID)) {
                        while (have_rows('seleziona_file', $post->ID)) {
                            the_row();
                            $file = get_sub_field('file');
                            ?>
                            <div <?php echo $pos_file ?>>
                                <a href="<?php echo $file['url']; ?>">
                                    <img style="margin-right:30px"
                                         src="<?php bloginfo('stylesheet_directory'); ?>/img/icon_file.png">
                                    <?php echo $file['filename']; ?>
                                </a>
                                <div style="margin-bottom:10px;"></div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}

?>



<div class="register_page">

    <div class="tml tml-profile" id="theme-my-login<?php $template->the_instance(); ?>">
        <div class="col-sm-12">
            <h1><?php echo get_field('titolo'); ?></h1>
            <p><?php echo get_field('testo'); ?></p>

        </div>
        <?php //$template->the_action_template_message( 'register' ); ?>
        <?php $template->the_errors(); ?>


        <form id="your-profile" action="<?php $template->the_action_url('profile', 'login_post'); ?>" method="post">
            <?php wp_nonce_field('update-user_'.$current_user->ID); ?>
            <?php
            //            $current_user      = wp_get_current_user();
            $all_meta_for_user = get_user_meta($current_user->ID);

            echo '<script type="text/javascript">';
            echo 'var provincia="'.$all_meta_for_user["provincia"][0].'";';
            echo 'var nazione="'.$all_meta_for_user["nazione"][0].'";';
            echo 'var professione="'.$all_meta_for_user["professione"][0].'";';
            echo '</script>';
            ?>
            <p>
                <input type="hidden" name="from" value="profile"/>
                <input type="hidden" name="checkuser_id" value="<?php echo $current_user->ID; ?>"/>
            </p>
            <div class="col-sm-12">
                <h1><?php echo __('Modifica dati utente');//_e( 'Personal Options', 'theme-my-login' ); ?></h1>
            </div>

            <p class="tml-user-login-wrap">
                <input type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>"
                       class="input" value="<?php echo $current_user->user_login; ?>" size="20"/>
            </p>
            <p class="tml-user-login-wrap">
                <input type="text" name="nickname" id="nickname" value="<?php echo $current_user->nickname; ?>"
                       class="regular-text"/>
            </p>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="first_name" id="first_name<?php $template->the_instance(); ?>"
                           class="input" value="<?php echo $all_meta_for_user['first_name'][0]; ?>" size="20"
                           placeholder="NOME*"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="last_name" id="last_name<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["last_name"][0]; ?>" size="20" placeholder="COGNOME*"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="azienda" id="azienda<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["azienda"][0]; ?>" size="20" placeholder="AZIENDA"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="email" id="email<?php $template->the_instance(); ?>"
                           class="input" value="<?php echo $current_user->user_email; ?>" size="20"
                           placeholder="E-MAIL*"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="indirizzo" id="indirizzo<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["indirizzo"][0]; ?>" size="20"
                           placeholder="INDIRIZZO"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="CAP" id="CAP<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["CAP"][0]; ?>" size="20" placeholder="CAP"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="citta" id="citta<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["citta"][0]; ?>" size="20" placeholder="CITT&Agrave;"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="select-style">
                    <select name="provincia">
                        <option value="">PROVINCIA</option>
                        <option value="AG">Agrigento</option>
                        <option value="AL">Alessandria</option>
                        <option value="AN">Ancona</option>
                        <option value="AO">Aosta</option>
                        <option value="AR">Arezzo</option>
                        <option value="AP">Ascoli Piceno</option>
                        <option value="AT">Asti</option>
                        <option value="AV">Avellino</option>
                        <option value="BA">Bari</option>
                        <option value="BL">Belluno</option>
                        <option value="BN">Benevento</option>
                        <option value="BG">Bergamo</option>
                        <option value="BI">Biella</option>
                        <option value="BO">Bologna</option>
                        <option value="BZ">Bolzano</option>
                        <option value="BS">Brescia</option>
                        <option value="BR">Brindisi</option>
                        <option value="CA">Cagliari</option>
                        <option value="CL">Caltanissetta</option>
                        <option value="CB">Campobasso</option>
                        <option value="CE">Caserta</option>
                        <option value="CT">Catania</option>
                        <option value="CZ">Catanzaro</option>
                        <option value="CH">Chieti</option>
                        <option value="CO">Como</option>
                        <option value="CS">Cosenza</option>
                        <option value="CR">Cremona</option>
                        <option value="KR">Crotone</option>
                        <option value="CN">Cuneo</option>
                        <option value="EN">Enna</option>
                        <option value="FE">Ferrara</option>
                        <option value="FI">Firenze</option>
                        <option value="FG">Foggia</option>
                        <option value="FC">Forlì-Cesena</option>
                        <option value="FR">Frosinone</option>
                        <option value="GE">Genova</option>
                        <option value="GO">Gorizia</option>
                        <option value="GR">Grosseto</option>
                        <option value="IM">Imperia</option>
                        <option value="IS">Isernia</option>
                        <option value="AQ">L'Aquila</option>
                        <option value="SP">La Spezia</option>
                        <option value="LT">Latina</option>
                        <option value="LE">Lecce</option>
                        <option value="LC">Lecco</option>
                        <option value="LI">Livorno</option>
                        <option value="LO">Lodi</option>
                        <option value="LU">Lucca</option>
                        <option value="MC">Macerata</option>
                        <option value="MN">Mantova</option>
                        <option value="MS">Massa-Carrara</option>
                        <option value="MT">Matera</option>
                        <option value="ME">Messina</option>
                        <option value="MI">Milano</option>
                        <option value="MO">Modena</option>
                        <option value="NA">Napoli</option>
                        <option value="NO">Novara</option>
                        <option value="NU">Nuoro</option>
                        <option value="OR">Oristano</option>
                        <option value="PD">Padova</option>
                        <option value="PA">Palermo</option>
                        <option value="PR">Parma</option>
                        <option value="PV">Pavia</option>
                        <option value="PG">Perugia</option>
                        <option value="PU">Pesaro e Urbino</option>
                        <option value="PE">Pescara</option>
                        <option value="PC">Piacenza</option>
                        <option value="PI">Pisa</option>
                        <option value="PT">Pistoia</option>
                        <option value="PN">Pordenone</option>
                        <option value="PZ">Potenza</option>
                        <option value="PO">Prato</option>
                        <option value="RG">Ragusa</option>
                        <option value="RA">Ravenna</option>
                        <option value="RC">Reggio Calabria</option>
                        <option value="RE">Reggio Emilia</option>
                        <option value="RI">Rieti</option>
                        <option value="RN">Rimini</option>
                        <option value="RM">Roma</option>
                        <option value="RO">Rovigo</option>
                        <option value="SA">Salerno</option>
                        <option value="SS">Sassari</option>
                        <option value="SV">Savona</option>
                        <option value="SI">Siena</option>
                        <option value="SR">Siracusa</option>
                        <option value="SO">Sondrio</option>
                        <option value="TA">Taranto</option>
                        <option value="TE">Teramo</option>
                        <option value="TR">Terni</option>
                        <option value="TO">Torino</option>
                        <option value="TP">Trapani</option>
                        <option value="TN">Trento</option>
                        <option value="TV">Treviso</option>
                        <option value="TS">Trieste</option>
                        <option value="UD">Udine</option>
                        <option value="VA">Varese</option>
                        <option value="VE">Venezia</option>
                        <option value="VB">Verbano-Cusio-Ossola</option>
                        <option value="VC">Vercelli</option>
                        <option value="VR">Verona</option>
                        <option value="VV">Vibo Valentia</option>
                        <option value="VI">Vicenza</option>
                        <option value="VT">Viterbo</option>
                    </select>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="select-style">
                    <select name="nazione">
                        <option selected="selected" value="">NAZIONE*</option>
                        <option value="AF">Afghanistan</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AG">Antigua and Barbuda</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AP">Asia/Pacific Region</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia</option>
                        <option value="BA">Bosnia and Herzegovina</option>
                        <option value="BW">Botswana</option>
                        <option value="BR">Brazil</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, The Democratic Republic of the</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Cote D'Ivoire</option>
                        <option value="HR">Croatia</option>
                        <option value="CU">Cuba</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="TP">East Timor</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran, Islamic Republic of</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakstan</option>
                        <option value="KE">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libyan Arab Jamahiriya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macau</option>
                        <option value="MK">Macedonia</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="ME">Montenegro</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PS">Palestinian Territory, Occupied</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Reunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="RS">Serbia</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SK">Slovakia</option>
                        <option value="SI">Slovenia</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH">Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela</option>
                        <option value="VN">Vietnam</option>
                        <option value="YE">Yemen</option>
                        <option value="ZR">Zaire</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
                    </select>
                </div>
                </p>
            </div>


            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="select-style">
                    <select name="professione">
                        <option selected="selected" value="">PROFESSIONE*</option>
                        <option value="RIVENDITORE">Rivenditore</option>
                        <option value="ARCHITETTO">Architetto / Interior Design</option>
                        <option value="PRIVATO">Privato</option>
                        <option value="ALTRO">Altro</option>
                    </select>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="telefono" id="telefono<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["telefono"][0]; ?>" size="20" placeholder="TELEFONO"/>
                </div>
                </p>
            </div>


            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input type="text" name="fax" id="fax<?php $template->the_instance(); ?>" class="input"
                           value="<?php echo $all_meta_for_user["fax"][0]; ?>" size="20" placeholder="FAX"/>
                </div>
                </p>
            </div>

            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input autocomplete="off" name="pass1" id="pass1<?php $template->the_instance(); ?>" class="input"
                           size="20" value="" type="password" placeholder="PASSWORD"/>
                </div>
                </p>
            </div>
            <div class="col-sm-6">
                <p class="tml-user-login-wrap">
                <div class="styled-select">
                    <input autocomplete="off" name="pass2" id="pass2<?php $template->the_instance(); ?>" class="input"
                           size="20" value="" type="password" placeholder="CONFERMA PASSWORD"/>
                </div>
                </p>
            </div>


            <!--<div class="col-sm-12">
                <p class="tml-user-login-wrap">
                <div>
                    <textarea rows="8" cols="50" name="comment" form="usrform"
                              placeholder="TESTO DEL MESSAGGIO"></textarea>
                </div>
                </p>
            </div>-->

            <?php //do_action( 'show_user_profile', $profileuser ); ?>

            <p class="tml-submit-wrap" style="margin-left: 50%; padding-left: 15px; padding-right: 15px;">
                <input type="hidden" name="action" value="profile"/>
                <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>"/>
                <input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr($current_user->ID); ?>"/>
                <input type="submit" value="<?php esc_attr_e('Update Profile', 'theme-my-login'); ?>" name="wp-submit"
                       id="wp-submit<?php $template->the_instance(); ?>" style="width:100%"/>
            </p>

        </form>
    </div>
</div>
<style>
.popularpost{
	overflow: hidden;
    border-bottom: 1px solid #BDBDBD;
    padding: 20px 0;
}
.popularpost img{
	width:100%;
	height:auto;
}

</style>
<section id="blog_sidebar">
<?php

$popularpost  = new WP_Query( array(  'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
if($popularpost->have_posts()){
?>
	<h4><?php echo __('I più letti'); ?></h4>
<?php 
	while ($popularpost->have_posts()){
	$popularpost->the_post();
	if( have_rows('elementi', $popularpost->ID) ):
    while ( have_rows('elementi', $popularpost->ID) ) : the_row(); 
	$image1 = get_sub_field('img1', $popularpost->ID);

?>

<a href="<?php echo the_permalink($popularpost->ID) ?>">
<div class="popularpost" <?php if (($popularpost->current_post +1) == ($popularpost->post_count)) {
  echo 'style="border:none;"';
} ?>>
	<div class="col-xs-5" style="padding:0;">
    	<?php echo wp_get_attachment_image( $image1, 'prod_gallery' ); ?>
    </div>
    <div class="col-xs-7" style="padding-right:0;">
    	<?php echo get_the_title($popularpost->ID) ?>
    </div>
</div>
</a>
<div style="clear:both;"></div>

<?php
endwhile;
endif;
} 
}
?>

<?php
$taxonomy     = 'category';
$orderby      = 'name';
$show_count   = false;
$pad_counts   = false;
$hierarchical = true;
$title        = '';

$args = array(
    'taxonomy'     => $taxonomy,
    'orderby'      => $orderby,
    'show_count'   => $show_count,
    'pad_counts'   => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li'     => $title,
    'hide_empty'   => true,
);
?>
<div style="border-top:1px solid #A46F25; margin-top:60px;"></div>
<h4><?php echo __('Categories'); ?></h4>
<ul>
    <?php 
	wp_list_categories($args); 
	?>
    
</ul>
</section>

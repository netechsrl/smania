<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_smania_it');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'masterkey');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vsmhxluzboi2ld6kiqdyzbtnqybi5q9bjziq6fwstcoe9chpgjwfqiysjrjxe7c7');
define('SECURE_AUTH_KEY',  '4wjrimaqha0pjydguvgddybbdbvbqpgrczqefvikvthf9f4f6solir1rrecvd9fh');
define('LOGGED_IN_KEY',    'v4dl2xnkvhhhnebnsettnwurocg93nvkthij1w58ex6r4wqbzsjdwtvpz6lmftdh');
define('NONCE_KEY',        '8anztyxxaeefktquxusgxdfhpylpclgse3g0h4ipcqpa2eyhutwtlsexgedbnsoz');
define('AUTH_SALT',        'ejpnbdharezmhj3hbztykusugxxi9mdjrese9bi4xlxxqbzmcdxfgbp6msdtffqm');
define('SECURE_AUTH_SALT', 'lzaogyysdt2wvqblzky9ccenz4wgvhyq2ijnrp8ogopjnbmzmokdxpejvzdr1zax');
define('LOGGED_IN_SALT',   'bkq7zkxls65jb5hvehv3nlp94cnh8wcf0pgyi0newrc8inb5zqzzx4jyx296bhth');
define('NONCE_SALT',       'qbtoxbjhrlem7yiili4mm7jga1bnfprwk7mj0hlwsrvh16010cvg7d9gelkzzibb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
